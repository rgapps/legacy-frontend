import React from 'react';
import FrontSide from '../FrontSide';
import {shallow} from 'enzyme';
import '../../../__tests/setupTests';
import 'jest-styled-components';

describe('<FrontSide />', () => {
  it('Should have 3 Children, (Example, Description and Type)', () => {
    const infoFront = {
      wordType: 'Verb',
      example: 'This is an example',
      description: 'This is a description',
      language: 'English',
    };

    const wrapper = shallow(<FrontSide front={infoFront} language={infoFront.language}/>);
    const content = wrapper.children();

    expect(content.length).toBe(3);
    expect(content.at(0).text()).toBe(infoFront.wordType);
    expect(content.at(1).text()).toContain(infoFront.description);
    expect(content.at(2).text()).toContain(infoFront.example);
  });
});
