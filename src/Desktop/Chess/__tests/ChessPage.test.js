import React from 'react';
import {mount} from 'enzyme';
import ChessPage from '../ChessPage';
import {waitExpectWithAct} from '../../../__tests/setupTests';
import '../../../__tests/setupTests';
import 'jest-styled-components';

describe('<ChessPage />', () => {
  it('Should redirect to Desk if click the table', async () => {
    const wrapper = mount(<ChessPage/>);
    wrapper.simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith(`/desktop`);
    });
  });
});
