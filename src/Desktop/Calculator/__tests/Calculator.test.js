import React from 'react';
import Calculator from '../Calculator';
import Button from '../Button';
import Display from '../Display';
import {mount, shallow} from 'enzyme';
import '../../../__tests/setupTests';
import 'jest-styled-components';

describe('<Calculator/>', () => {
  it('Should exist 19 buttons', () => {
    const wrapper = shallow(<Calculator/>);
    const buttons = wrapper.find(Button);

    expect(buttons.length).toBe(19);
  });

  it('All numbers button should work properly', async () => {
    const wrapper = mount(<Calculator/>);
    const buttons = wrapper.find(Button);
    const display = wrapper.find(Display);

    const button7 = buttons.at(0);
    const button8 = buttons.at(1);
    const button9 = buttons.at(2);
    const button4 = buttons.at(5);
    const button5 = buttons.at(6);
    const button6 = buttons.at(7);
    const button1 = buttons.at(10);
    const button2 = buttons.at(11);
    const button3 = buttons.at(12);
    const button0 = buttons.at(15);
    const buttonDot = buttons.at(16);

    button1.simulate('click');
    button2.simulate('click');
    button3.simulate('click');
    button4.simulate('click');
    button5.simulate('click');
    button6.simulate('click');
    button7.simulate('click');
    button8.simulate('click');
    button9.simulate('click');
    button0.simulate('click');
    buttonDot.simulate('click');
    expect(display.text()).toBe('1234567890.');
  });

  it(`Shouldn't render dot more than two times`, () => {
    const wrapper = mount(<Calculator/>);
    const buttons = wrapper.find(Button);
    const dot = buttons.at(16);
    const num = buttons.at(10);
    const display = wrapper.find(Display);

    num.simulate('click');
    expect(display.text()).toBe(num.prop('value'));
    dot.simulate('click');
    expect(display.text()).toBe(num.prop('value') + dot.prop('value'));
    num.simulate('click');
    expect(display.text()).toBe(num.prop('value') + dot.prop('value') + num.prop('value'));
    dot.simulate('click');
    expect(display.text()).toBe(num.prop('value') + dot.prop('value') + num.prop('value'));
  });

  it('Should render "0." when showing the result', () => {
    const wrapper = mount(<Calculator/>);
    const buttons = wrapper.find(Button);
    const display = wrapper.find(Display);

    const dot = buttons.at(16);
    const addition = buttons.at(13);
    const button1 = buttons.at(10);

    button1.simulate('click');
    dot.simulate('click');
    button1.simulate('click');
    expect(display.text()).toBe('1.1');
    addition.simulate('click');
    dot.simulate('click');
    expect(display.text()).toBe('0.');
  });

  it('functions should work properly', async () => {
    const wrapper = mount(<Calculator/>);
    const buttons = wrapper.find(Button);
    const display = wrapper.find(Display);

    const addition = buttons.at(13);
    const substraction = buttons.at(14);
    const multiplication = buttons.at(8);
    const division = buttons.at(9);
    const buttonClearCurrent = buttons.at(3);
    const buttonClearGlobal = buttons.at(17);
    const num1 = buttons.at(1);
    const num2 = buttons.at(6);
    const equalTo = buttons.at(18);
    const changeSign = buttons.at(4);

    num2.simulate('click');
    expect(display.text()).toBe(num2.prop('value'));
    addition.simulate('click');
    expect(display.text()).toBe(num2.prop('value'));
    num1.simulate('click');
    expect(display.text()).toBe(num1.prop('value'));
    substraction.simulate('click');
    expect(display.text()).toBe('13');
    num2.simulate('click');
    expect(display.text()).toBe(num2.prop('value'));
    multiplication.simulate('click');
    expect(display.text()).toBe('8');
    num1.simulate('click');
    expect(display.text()).toBe(num1.prop('value'));
    division.simulate('click');
    expect(display.text()).toBe('64');
    num2.simulate('click');
    expect(display.text()).toBe(num2.prop('value'));
    addition.simulate('click');
    expect(display.text()).toBe('12.8');
    num2.simulate('click');
    buttonClearCurrent.simulate('click');
    expect(display.text()).toBe('0');
    num2.simulate('click');
    expect(display.text()).toBe('5');
    addition.simulate('click');
    expect(display.text()).toBe('17.8');
    buttonClearCurrent.simulate('click');
    expect(display.text()).toBe('0');
    buttonClearGlobal.simulate('click');
    expect(display.text()).toBe('0');
    num1.simulate('click');
    num2.simulate('click');
    expect(display.text()).toBe('85');
    substraction.simulate('click');
    num1.simulate('click');
    num2.simulate('click');
    equalTo.simulate('click');
    expect(display.text()).toBe('0');
    addition.simulate('click');
    num2.simulate('click');
    equalTo.simulate('click');
    expect(display.text()).toBe('5');
    changeSign.simulate('click');
    expect(display.text()).toBe('5');
    addition.simulate('click');
    num2.simulate('click');
    expect(display.text()).toBe('5');
    changeSign.simulate('click');
    expect(display.text()).toBe('-5');
    equalTo.simulate('click');
    expect(display.text()).toBe('0');
  });

  it('should change fSize when more than 13 and 16 digits are on display', () => {
    const wrapper = mount(<Calculator/>);

    const display = wrapper.find(Display);
    const buttons = wrapper.find(Button);
    expect(display.prop('fSize')).toBe('40');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(1).simulate('click');
    buttons.at(17).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    expect(display.text()).toBe('999999999999999');
    buttons.at(8).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    buttons.at(2).simulate('click');
    expect(display.text()).toBe('999999999999999');
    buttons.at(18).simulate('click');
    expect(display.text()).toBe('9.99999999999998e+29');
  });
});
