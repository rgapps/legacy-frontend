import React from 'react';
import {mount} from 'enzyme';
import '../../../__tests/setupTests';
import 'jest-styled-components';
import StockJournalPage from '../StockJournalPage';
import {mockedServer, rest, waitExpectWithAct} from '../../../__tests/setupTests';
import {Endpoints} from '../../../endpoints';
import {defaultAccount, stockJournalRow} from '../../__tests/modelData';
import LockingError from '../../../Common/Components/LockingError';

describe('<StockJournalPage />', () => {
  it('Should be displayed correctly', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stocksJournal(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));

    const wrapper = mount(<StockJournalPage/>);

    expect(wrapper).toBeTruthy();

    expect(wrapper.find('h1').text()).toBe('STOCKS JOURNAL');
    const buttons = wrapper.find('button');
    expect(buttons).toHaveLength(1);
    expect(buttons.at(0).text()).toBe('Go to Transactions');

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(2);
    });
  });

  it('Should fail and not show table when journal data fails', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stocksJournal(defaultAccount.id), (req, res) => {
      return res.networkError('Error Message');
    }));

    const wrapper = mount(<StockJournalPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(2);
    });

    await waitExpectWithAct(() => {
      const test = wrapper.update().find(LockingError);
      expect(test.text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should display table when journal data is received', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stocksJournal(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([{
        stockCode: 'ABC',
        averagePrice: 1.0,
        numberOfShares: 1,
        boughtDate: '2020-01-01',
        marketValue: 10.0,
        currency: 'AED',
      }]));
    }));

    const wrapper = mount(<StockJournalPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(2);
    });

    await waitExpectWithAct(() => {
      const trs = wrapper.update().find('th');
      expect(trs).toHaveLength(5);
      expect(trs.at(0).text().trim()).toBe('Stock Code');
      expect(trs.at(1).text().trim()).toBe('Avg Price');
      expect(trs.at(2).text().trim()).toBe('No Shares');
      expect(trs.at(3).text().trim()).toBe('Bought Date');
      expect(trs.at(4).text().trim()).toBe('Market Value');
    });

    await waitExpectWithAct(() => {
      const tds = wrapper.update().find('td');
      expect(tds).toHaveLength(7);
      expect(tds.at(0).text().trim()).toBe('ABC');
      expect(tds.at(1).text().trim()).toBe('1.00 AED');
      expect(tds.at(2).text().trim()).toBe('1');
      expect(tds.at(3).text().trim()).toBe('2020-01-01');
      expect(tds.at(4).text().trim()).toBe('10.00 AED');
      expect(tds.at(5).text().trim()).toBe('Total Equity');
      expect(tds.at(6).text().trim()).toBe('10.00');
    });
  });

  it('Should redirect to Transactions when go to Transactions is clicked', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stocksJournal(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockJournalRow]));
    }));

    const wrapper = mount(<StockJournalPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(2);
    });

    const HistoryButton = wrapper.find('button').at(0);
    HistoryButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith(Endpoints.stockTransactionsPage());
    });
  });
});
