|  |                                                                                                                                                          |                                                                                                                                                          |
|--------|:--------------------------------------------------------------------------------------------------------------------------------------------------------:|---------------------------------------------------------------------------------------------------------------------------------------------------------:|
| Master | [![pipeline status](https://gitlab.com/rgapps/desktop-frontend/badges/master/pipeline.svg)](https://gitlab.com/rgapps/desktop-frontend/-/commits/master) | [![coverage report](https://gitlab.com/rgapps/desktop-frontend/badges/master/coverage.svg)](https://gitlab.com/rgapps/desktop-frontend/-/commits/master) |
| Dev    |  [![pipeline status](https://gitlab.com/rgapps/desktop-frontend/badges/dev/pipeline.svg)   ](https://gitlab.com/rgapps/desktop-frontend/-/commits/dev)   |       [![coverage report](https://gitlab.com/rgapps/desktop-frontend/badges/dev/coverage.svg)](https://gitlab.com/rgapps/desktop-frontend/-/commits/dev) |

# Useful commands

### Run database

``` 
docker-compose -f cicd/docker-compose.yml --project-directory . up -d database
```

### Run backend with docker compose

``` 
docker-compose -f cicd/docker-compose.yml --project-directory . up -d general-backend
```

### Run locally

- Run the database and backend
- To deploy locally just run `npm start`

### build docker

```
docker build -f cicd/build/Dockerfile .
```

### build with docker compose

```
docker-compose -f cicd/docker-compose.yml --project-directory . build desktop-frontend
```

### run all with docker compose

``` 
docker-compose -f cicd/docker-compose.yml --project-directory . up -d
``` 

> *NOTE:* run these commands from the project directory