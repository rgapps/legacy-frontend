import React from 'react';
import LanguageCardsPage from '../LanguageCardsPage';
import {mount} from 'enzyme';
import {waitExpectWithAct} from '../../../__tests/setupTests';
import {AnimationType, defaultAnimationBehavior} from '../../Animations/Behavior/behaviorDefinitions';
import {AnimationBehaviorContext} from '../../Animations/Behavior/AnimationBehaviorHandler';
import '../../../__tests/setupTests';
import 'jest-styled-components';

describe('<LanguageCards />', () => {
  it('Should redirect to Desk if click the table', async () => {
    const wrapper = mount(<LanguageCardsPage/>);
    wrapper.simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith(`/desktop`);
    });
  });

  it('Dont redirect to desktop if Draggable animation is running', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      runningAny: (type) => type === AnimationType.Drag,
    };
    const wrapper = mount(<AnimationBehaviorContext.Provider value={animationBehavior}>
      <LanguageCardsPage/>
    </AnimationBehaviorContext.Provider>,
    );

    wrapper.find(LanguageCardsPage).simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).not.toBeCalled();
    });
  });

  it('Should show in english if language is "English"', () => {
    let language = 'English';
    const wrapper = mount(<LanguageCardsPage language={language}/>);
    expect(wrapper.text()).toContain('Verb');
    language = 'Spanish';
    const secondWrapper = mount(<LanguageCardsPage language={language}/>);
    expect(secondWrapper.text()).toContain('Verbo');
  });
});
