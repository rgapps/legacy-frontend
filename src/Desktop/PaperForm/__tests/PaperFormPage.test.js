import React from 'react';
import {mount, shallow} from 'enzyme';
import '../../../__tests/setupTests';
import PaperFormPage, {Drawer, Trash} from '../PaperFormPage';
import PaperForm from '../PaperForm';
import {expectToThrow, mockedServer, rest, waitExpectWithAct} from '../../../__tests/setupTests';
import each from 'jest-each';
import {act} from '@testing-library/react';
import Draggable from '../../../Desktop/Animations/Draggable';
import {Endpoints} from '../../../endpoints';
import LockingError from '../../../Common/Components/LockingError';
import NotificationError from '../../../Common/Components/NotificationError';
import {defaultAccount} from '../../../Stocks/__tests/modelData';

describe('<PaperFormPage/>', () => {
  it('fails when no match is passed', () => {
    expectToThrow(() => {
      shallow(<PaperFormPage/>);
    }, 'Warning: Failed prop type: The prop `match` is marked as required in `PaperFormPage`, but its value is `undefined`.');
  });

  each([
    [{other: 'other'}, 'match.params'],
    [{params: {other: 'other'}}, 'match.params.name'],
  ]).it('fails when match doesnt fit shape is passed', (match, field) => {
    expectToThrow(() => shallow(<PaperFormPage match={match}/>),
      `Warning: Failed prop type: The prop \`${field}\` is marked as required in \`PaperFormPage\`, but its value is \`undefined\`.`);
  });

  it('renders with paper form, and drawer', () => {
    const wrapper = shallow(<PaperFormPage match={{params: {name: 'name'}}}/>);

    const paperForm = wrapper.find(PaperForm);
    expect(paperForm.exists()).toBeTruthy();
    const drawer = wrapper.find(Drawer);
    expect(drawer.exists()).toBeTruthy();
  });

  it('click in form background redirects to desktop if no changes has been taken', async () => {
    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);

    wrapper.childAt(0).simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith('/desktop');
    });
  });

  it('dont redirect to desktop if clicks background when changes has been taken', async () => {
    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);
    await act(async () => {
      wrapper.find(PaperForm).props().setField('fieldChanged', 'value');
    });
    wrapper.childAt(0).simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).not.toBeCalled();
      expect(window.alert).toBeCalledWith('Save the changes before returning to the desktop!');
    });
  });

  it('dont redirect to desktop if clicks background when dragging', async () => {
    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);

    const draggable = wrapper.find(Draggable);
    await act(async () => {
      draggable.props().onDragStart();
    });

    wrapper.find(PaperFormPage).childAt(0).simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).not.toBeCalled();
    });
  });

  it('redirect to desktop if clicks background when not dragging anymore', async () => {
    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);

    const draggable = wrapper.find(Draggable);
    await act(async () => {
      draggable.props().onDragStart();
    });
    await act(async () => {
      draggable.props().onDragged();
    });

    wrapper.find(PaperFormPage).childAt(0).simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith('/desktop');
    });
  });

  it('save form correctly dragging', async () => {
    mockedServer.use(rest.post(Endpoints.buyStockTransaction(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json({}));
    }));

    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);

    wrapper.find('input').at(0).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(1).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(2).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(3).simulate('change', {target: {value: 'test'}});
    await waitExpectWithAct(() => {
      expect(wrapper.find('input').at(0).prop('value')).toBe('test');
      expect(wrapper.find('input').at(1).prop('value')).toBe('test');
      expect(wrapper.find('input').at(2).prop('value')).toBe('test');
      expect(wrapper.find('input').at(3).prop('value')).toBe('test');
    });

    const draggable = wrapper.update().find(Draggable);
    await act(async () => {
      draggable.props().onDragStart();
    });

    await act(async () => {
      const drawerRef = wrapper.find(Drawer).getNodeInternal().ref.current;
      draggable.props().onDragEnd({target: drawerRef});
    });
    await waitExpectWithAct(() => {
      wrapper.update();
      expect(wrapper.find('input').at(0).prop('value')).toBe('');
      expect(wrapper.find('input').at(1).prop('value')).toBe('');
      expect(wrapper.find('input').at(2).prop('value')).toBe('');
      expect(wrapper.find('input').at(3).prop('value')).toBe('');
    });
    expect(mockedServer.receivedCalls()).toBe(1);
  });

  it('save form correctly clicking', async () => {
    mockedServer.use(rest.post(Endpoints.buyStockTransaction(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json({}));
    }));
    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);

    wrapper.find('input').at(0).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(1).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(2).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(3).simulate('change', {target: {value: 'test'}});
    await waitExpectWithAct(() => {
      expect(wrapper.find('input').at(0).prop('value')).toBe('test');
      expect(wrapper.find('input').at(1).prop('value')).toBe('test');
      expect(wrapper.find('input').at(2).prop('value')).toBe('test');
      expect(wrapper.find('input').at(3).prop('value')).toBe('test');
    });

    wrapper.find(Drawer).simulate('click');
    await waitExpectWithAct(() => {
      wrapper.update();
      expect(wrapper.find('input').at(0).prop('value')).toBe('');
      expect(wrapper.find('input').at(1).prop('value')).toBe('');
      expect(wrapper.find('input').at(2).prop('value')).toBe('');
      expect(wrapper.find('input').at(3).prop('value')).toBe('');
    });
    expect(mockedServer.receivedCalls()).toBe(1);
  });

  it('show errors if drawer fails with network error while archiving', async () => {
    mockedServer.use(rest.post(Endpoints.buyStockTransaction(defaultAccount.id), (req, res) => {
      return res.networkError('Network error example');
    }));
    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);

    const draggable = wrapper.find(Draggable);
    await act(async () => {
      draggable.props().onDragStart();
    });

    await act(async () => {
      const drawerRef = wrapper.find(Drawer).getNodeInternal().ref.current;
      draggable.props().onDragEnd({target: drawerRef});
    });
    await waitExpectWithAct(() => {
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
    expect(mockedServer.receivedCalls()).toBe(1);
  });

  it('show errors if drawer fails with field validation error while archiving', async () => {
    mockedServer.use(rest.post(Endpoints.buyStockTransaction(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(400), ctx.json({
        message: 'Error Message',
        errors: {'field1': ['error1', 'error2'], 'field2': ['error1', 'error2']},
      }));
    }));

    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);

    const draggable = wrapper.find(Draggable);
    await act(async () => {
      draggable.props().onDragStart();
    });

    await act(async () => {
      const drawerRef = wrapper.find(Drawer).getNodeInternal().ref.current;
      draggable.props().onDragEnd({target: drawerRef});
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(NotificationError).find('strong').text()).toBe('Error Message');
      expect(wrapper.update().find(NotificationError).text()).toContain('field1 error1,error2');
      expect(wrapper.update().find(NotificationError).text()).toContain('field2 error1,error2');
    });
    expect(mockedServer.receivedCalls()).toBe(1);
  });

  it('show errors if drawer fails with handled error while archiving', async () => {
    mockedServer.use(rest.post(Endpoints.buyStockTransaction(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(400), ctx.json({message: 'Error Message'}));
    }));

    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);

    const draggable = wrapper.find(Draggable);
    await act(async () => {
      draggable.props().onDragStart();
    });

    await act(async () => {
      const drawerRef = wrapper.find(Drawer).getNodeInternal().ref.current;
      draggable.props().onDragEnd({target: drawerRef});
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(NotificationError).find('strong').text()).toBe('Error Message');
      expect(wrapper.update().find(NotificationError).text()).toContain('Error Message');
    });
    expect(mockedServer.receivedCalls()).toBe(1);
  });

  it('clear fields with trash dragging', async () => {
    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);
    wrapper.find('input').at(0).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(1).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(2).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(3).simulate('change', {target: {value: 'test'}});
    await waitExpectWithAct(() => {
      expect(wrapper.find('input').at(0).prop('value')).toBe('test');
      expect(wrapper.find('input').at(1).prop('value')).toBe('test');
      expect(wrapper.find('input').at(2).prop('value')).toBe('test');
      expect(wrapper.find('input').at(3).prop('value')).toBe('test');
    });

    const draggable = wrapper.find(Draggable);
    await act(async () => {
      draggable.props().onDragStart();
    });

    await act(async () => {
      const trashRef = wrapper.find(Trash).getNodeInternal().ref.current;
      draggable.props().onDragEnd({target: trashRef});
    });
    await waitExpectWithAct(() => {
      wrapper.update();
      expect(wrapper.find('input').at(0).prop('value')).toBe('');
      expect(wrapper.find('input').at(1).prop('value')).toBe('');
      expect(wrapper.find('input').at(2).prop('value')).toBe('');
      expect(wrapper.find('input').at(3).prop('value')).toBe('');
    });
  });

  it('clear fields with trash clicking', async () => {
    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);
    wrapper.find('input').at(0).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(1).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(2).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(3).simulate('change', {target: {value: 'test'}});
    await waitExpectWithAct(() => {
      expect(wrapper.find('input').at(0).prop('value')).toBe('test');
      expect(wrapper.find('input').at(1).prop('value')).toBe('test');
      expect(wrapper.find('input').at(2).prop('value')).toBe('test');
      expect(wrapper.find('input').at(3).prop('value')).toBe('test');
    });

    wrapper.find(Trash).simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(wrapper.find('input').at(0).prop('value')).toBe('');
      expect(wrapper.find('input').at(1).prop('value')).toBe('');
      expect(wrapper.find('input').at(2).prop('value')).toBe('');
      expect(wrapper.find('input').at(3).prop('value')).toBe('');
    });
  });

  it('dont save not clean if draw to other element different than the Drawer', async () => {
    const unknownRef = React.createRef();
    mount(<div ref={unknownRef}/>);
    const wrapper = mount(<PaperFormPage match={{params: {name: 'name'}}}/>);

    wrapper.find('input').at(0).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(1).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(2).simulate('change', {target: {value: 'test'}});
    wrapper.find('input').at(3).simulate('change', {target: {value: 'test'}});
    await waitExpectWithAct(() => {
      expect(wrapper.find('input').at(0).prop('value')).toBe('test');
      expect(wrapper.find('input').at(1).prop('value')).toBe('test');
      expect(wrapper.find('input').at(2).prop('value')).toBe('test');
      expect(wrapper.find('input').at(3).prop('value')).toBe('test');
    });

    const draggable = wrapper.find(Draggable);
    await act(async () => {
      draggable.props().onDragStart();
    });

    await act(async () => {
      draggable.props().onDragEnd({target: unknownRef});
    });

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(wrapper.find('input').at(0).prop('value')).toBe('test');
      expect(wrapper.find('input').at(1).prop('value')).toBe('test');
      expect(wrapper.find('input').at(2).prop('value')).toBe('test');
      expect(wrapper.find('input').at(3).prop('value')).toBe('test');
    });
  });
});
