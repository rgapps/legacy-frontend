import React from 'react';
import {mount} from 'enzyme';
import '../../../__tests/setupTests';
import 'jest-styled-components';
import StockTransactionsPage from '../StockTransactionsPage';
import {Actions} from '../ActionsMenu';
import {mockedServer, rest, waitExpectWithAct} from '../../../__tests/setupTests';
import {Endpoints} from '../../../endpoints';
import {
  accountDepositRow,
  accountSummary,
  accountWithdrawRow,
  apiError,
  defaultAccount,
  stockBuyTransactionRow,
  stockDividendTransactionRow,
  stockSellTransactionRow,
} from '../../__tests/modelData';
import LockingError from '../../../Common/Components/LockingError';
import NotificationError from './../../../Common/Components/NotificationError';

describe('<StockTransactionsPage />', () => {
  it('Should be displayed correctly', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    expect(wrapper.find('h1').text()).toBe('STOCK TRANSACTIONS');
    const buttons = wrapper.find('button');
    expect(buttons).toHaveLength(8);
    expect(buttons.at(0).text()).toBe('Journal');
    expect(buttons.at(1).text()).toBe('Profit History');
    expect(buttons.at(2).text()).toBe('Dividend');
    expect(buttons.at(3).text()).toBe('Buy');
    expect(buttons.at(4).text()).toBe('Sell');
    expect(buttons.at(5).text()).toBe(''); // undo
    expect(buttons.at(6).text()).toBe('Deposit');
    expect(buttons.at(7).text()).toBe('Withdraw');

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });
  });

  it('Should fail when transaction data fails', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res) => {
      return res.networkError('error message');
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should fail when account deposits data fails', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res) => {
      return res.networkError('error message');
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(4);
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should fail when accounts data fails', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res.networkError('error message');
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should display transactions, movements and account summary when data is received', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow, stockSellTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([accountDepositRow, accountWithdrawRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    await waitExpectWithAct(() => {
      const accountSummary = wrapper.update().first('#AccountSummary').text();
      expect(accountSummary).toContain('Buying Power');
      expect(accountSummary).toContain('2020-12-07');
      expect(accountSummary).toContain('12,000.00');
      expect(accountSummary).toContain('PHP');

      const trs = wrapper.find('th');
      expect(trs).toHaveLength(7);
      expect(trs.at(0).text().trim()).toBe('Date');
      expect(trs.at(1).text().trim()).toBe('Stock Code');
      expect(trs.at(2).text().trim()).toBe('Shares');
      expect(trs.at(3).text().trim()).toBe('Per Stock');
      expect(trs.at(4).text().trim()).toBe('Fees');
      expect(trs.at(5).text().trim()).toBe('Adjusted Amount');
      expect(trs.at(6).text().trim()).toBe('Total Movement');

      const tds = wrapper.find('td');
      expect(tds).toHaveLength(28);
      expect(tds.at(0).text().trim()).toBe('2020-01-04');
      expect(tds.at(1).text().trim()).toBe('--');
      expect(tds.at(2).text().trim()).toBe('--');
      expect(tds.at(3).text().trim()).toBe('--');
      expect(tds.at(4).text().trim()).toBe('--');
      expect(tds.at(5).text().trim()).toBe('--');
      expect(tds.at(6).text().trim()).toBe('100.00 AED');
      expect(tds.at(7).text().trim()).toBe('2020-01-03');
      expect(tds.at(8).text().trim()).toBe('ABC');
      expect(tds.at(9).text().trim()).toBe('1');
      expect(tds.at(10).text().trim()).toBe('10.00 AED');
      expect(tds.at(11).text().trim()).toBe('1.00 AED');
      expect(tds.at(12).text().trim()).toBe('9.00 AED');
      expect(tds.at(13).text().trim()).toBe('9.00 AED');
      expect(tds.at(14).text().trim()).toBe('2020-01-02');
      expect(tds.at(15).text().trim()).toBe('ABC');
      expect(tds.at(16).text().trim()).toBe('1');
      expect(tds.at(17).text().trim()).toBe('10.00 AED');
      expect(tds.at(18).text().trim()).toBe('1.00 AED');
      expect(tds.at(19).text().trim()).toBe('11.00 AED');
      expect(tds.at(20).text().trim()).toBe('11.00 AED');
      expect(tds.at(21).text().trim()).toBe('2020-01-01');
      expect(tds.at(22).text().trim()).toBe('--');
      expect(tds.at(23).text().trim()).toBe('--');
      expect(tds.at(24).text().trim()).toBe('--');
      expect(tds.at(25).text().trim()).toBe('--');
      expect(tds.at(26).text().trim()).toBe('--');
      expect(tds.at(27).text().trim()).toBe('100.00 AED');
    });
  });

  it('Should redirect to Journal when go to Journal is clicked', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    expect(wrapper).toBeTruthy();

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const gotoJournalButton = wrapper.find('button').at(0);
    gotoJournalButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith(Endpoints.stockJournalPage());
    });
  });

  it('Should redirect to MonthlySummary when go to Summary is clicked', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const gotoStocksProfitsSummary = wrapper.find('button').at(1);
    gotoStocksProfitsSummary.simulate('click');

    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith(Endpoints.stockProfitSummaryPage());
    });
  });

  it('Should display dividend form when dividend button is clicked', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const dividendButton = wrapper.find('button').at(2);
    dividendButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5);
    });

    await waitExpectWithAct(() => {
      const inputs = wrapper.update().find('input');
      // TODO: Add check of the input labels
      expect(inputs).toHaveLength(5);
      expect(inputs.at(0).prop('value')).toBe(''); // Stock
      expect(inputs.at(1).prop('value')).toBe(''); // Price
      expect(inputs.at(2).prop('value')).toBe(''); // No Shares
      expect(inputs.at(3).prop('value')).toBe(''); // fee
      expect(inputs.at(4).prop('value')).toBe((new Date()).toISOString().split('T')[0]); // Date

      expect(wrapper.find('button').at(8).text()).toBe('Get Dividend');
    });
  });

  it('Should fail when get Stocks fail on dividend', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res) => {
      return res.networkError('error');
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const dividendButton = wrapper.find('button').at(2);
    dividendButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(5);
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should hide dividend form when dividend button is clicked again', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const dividendButton = wrapper.find('button').at(2);
    dividendButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find('input')).toHaveLength(5);
      expect(mockedServer.receivedCalls()).toBe(5);
    });

    dividendButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find('input')).toHaveLength(0);
    });
  });

  it('Should update table and account summary when a dividend is gotten', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    let stocksTransactionsCall = 0;
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      stocksTransactionsCall += 1;
      if (stocksTransactionsCall === 1) {
        return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
      } else {
        return res(ctx.status(200), ctx.json([stockBuyTransactionRow,
          {
            ...stockDividendTransactionRow,
            date: '2021-01-01',
            stockCode: 'DEF',
          }]));
      }
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.post(Endpoints.dividendTransaction(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json({}));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    let accountSummaryCall = 0;
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      accountSummaryCall += 1;
      if (accountSummaryCall === 1) {
        return res(ctx.status(200), ctx.json(accountSummary));
      } else {
        return res(ctx.status(200), ctx.json({
          ...accountSummary,
          lastDepositOrWithdrawalDate: '2021-01-01',
          amount: 11999,
        }));
      }
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const dividendButton = wrapper.find('button').at(2);
    dividendButton.simulate('click');
    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5); // get stocks
    });

    wrapper.find('input').at(0).simulate('change', {target: {value: 'DEF'}}); // Stock
    wrapper.find('input').at(1).simulate('change', {target: {value: 1}}); // No Shares
    wrapper.find('input').at(2).simulate('change', {target: {value: 1}}); // Price
    wrapper.find('input').at(3).simulate('change', {target: {value: 1}}); // fee
    wrapper.find('input').at(4).simulate('change', {target: {value: '2021-01-01'}}); // date
    const actualDividendButton = wrapper.find('button').at(8);
    actualDividendButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(8); // dividend, update transaction and stocks

      const inputs = wrapper.update().find('input');
      expect(inputs).toHaveLength(0);

      const accountSummary = wrapper.update().first('#AccountSummary').text();
      expect(accountSummary).toContain('Buying Power');
      expect(accountSummary).toContain('2021-01-01');
      expect(accountSummary).toContain('11,999.00');
      expect(accountSummary).toContain('PHP');

      const trs = wrapper.update().find('th');
      expect(trs).toHaveLength(7);
      expect(trs.at(0).text().trim()).toBe('Date');
      expect(trs.at(1).text().trim()).toBe('Stock Code');
      expect(trs.at(2).text().trim()).toBe('Shares');
      expect(trs.at(3).text().trim()).toBe('Per Stock');
      expect(trs.at(4).text().trim()).toBe('Fees');
      expect(trs.at(5).text().trim()).toBe('Adjusted Amount');
      expect(trs.at(6).text().trim()).toBe('Total Movement');

      const tds = wrapper.update().find('td');
      expect(tds).toHaveLength(14);
      expect(tds.at(0).text().trim()).toBe('2021-01-01');
      expect(tds.at(1).text().trim()).toBe('DEF');
      expect(tds.at(2).text().trim()).toBe('1');
      expect(tds.at(3).text().trim()).toBe('10.00 AED');
      expect(tds.at(4).text().trim()).toBe('1.00 AED');
      expect(tds.at(5).text().trim()).toBe('9.00 AED');
      expect(tds.at(6).text().trim()).toBe('9.00 AED');
      expect(tds.at(7).text().trim()).toBe('2020-01-02');
      expect(tds.at(8).text().trim()).toBe('ABC');
      expect(tds.at(9).text().trim()).toBe('1');
      expect(tds.at(10).text().trim()).toBe('10.00 AED');
      expect(tds.at(11).text().trim()).toBe('1.00 AED');
      expect(tds.at(12).text().trim()).toBe('11.00 AED');
      expect(tds.at(13).text().trim()).toBe('11.00 AED');
    });
  });

  it('Should show error message when dividend Stocks fail', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.post(Endpoints.dividendTransaction(defaultAccount.id), (req, res) => {
      return res.networkError('Error');
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transaction and accounts
    });

    const dividendButton = wrapper.find('button').at(2);
    dividendButton.simulate('click');
    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5); // get stocks
    });

    wrapper.find('input').at(0).simulate('change', {target: {value: 'DEF'}}); // Stock
    wrapper.find('input').at(1).simulate('change', {target: {value: 1}}); // Price
    wrapper.find('input').at(2).simulate('change', {target: {value: 1}}); // No Shares
    wrapper.find('input').at(3).simulate('change', {target: {value: 1}}); // brokerageFee
    wrapper.find('input').at(4).simulate('change', {target: {value: '2021-01-01'}}); // date
    const actualSellButton = wrapper.find('button').at(8);
    actualSellButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(6); // dividend
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should display buy form when buy button is clicked', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const buyButton = wrapper.find('button').at(3);
    buyButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5);
    });

    await waitExpectWithAct(() => {
      const inputs = wrapper.update().find('input');
      // TODO: Add check of the input labels
      expect(inputs).toHaveLength(5);
      expect(inputs.at(0).prop('value')).toBe(''); // Stock
      expect(inputs.at(1).prop('value')).toBe(''); // Price
      expect(inputs.at(2).prop('value')).toBe(''); // No Shares
      expect(inputs.at(3).prop('value')).toBe(''); // brokerageFee
      expect(inputs.at(4).prop('value')).toBe((new Date()).toISOString().split('T')[0]); // Date

      expect(wrapper.find('button').at(8).text()).toBe('Buy');
    });
  });

  it('Should fail when get Stocks fail on buy', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res) => {
      return res.networkError('error');
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const buyButton = wrapper.find('button').at(3);
    buyButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5);
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should hide buy form when buy button is clicked again', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const buyButton = wrapper.find('button').at(3);
    buyButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find('input')).toHaveLength(5);
      expect(mockedServer.receivedCalls()).toBe(5);
    });

    await waitExpectWithAct(() => {
      buyButton.simulate('click');
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find('input')).toHaveLength(0);
    });
  });

  it('Should update table and account summary when a transaction is bought', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    let stocksJournalCall = 0;
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      stocksJournalCall += 1;
      if (stocksJournalCall === 1) {
        return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
      } else {
        return res(ctx.status(200), ctx.json([stockBuyTransactionRow,
          {
            ...stockBuyTransactionRow,
            date: '2021-01-01',
            stockCode: 'DEF',
          }]));
      }
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.post(Endpoints.buyStockTransaction(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json({}));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    let accountSummaryCall = 0;
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      accountSummaryCall += 1;
      if (accountSummaryCall === 1) {
        return res(ctx.status(200), ctx.json(accountSummary));
      } else {
        return res(ctx.status(200), ctx.json({
          ...accountSummary,
          lastDepositOrWithdrawalDate: '2021-01-01',
          amount: 12001,
        }));
      }
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const buyButton = wrapper.find('button').at(3);
    buyButton.simulate('click');
    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5); // get stock
    });

    wrapper.find('input').at(0).simulate('change', {target: {value: 'DEF'}}); // Stock
    wrapper.find('input').at(1).simulate('change', {target: {value: 1}}); // Price
    wrapper.find('input').at(2).simulate('change', {target: {value: 1}}); // No Shares
    wrapper.find('input').at(3).simulate('change', {target: {value: 1}}); // brokerageFee
    wrapper.find('input').at(4).simulate('change', {target: {value: '2021-01-01'}}); // date
    const actualBuyButton = wrapper.find('button').at(8);
    actualBuyButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(8); // buy, update transactions and account

      const inputs = wrapper.update().find('input');
      expect(inputs).toHaveLength(0);

      const accountSummary = wrapper.update().first('#AccountSummary').text();
      expect(accountSummary).toContain('Buying Power');
      expect(accountSummary).toContain('2021-01-01');
      expect(accountSummary).toContain('12,001.00');
      expect(accountSummary).toContain('PHP');

      const trs = wrapper.find('th');
      expect(trs).toHaveLength(7);
      expect(trs.at(0).text().trim()).toBe('Date');
      expect(trs.at(1).text().trim()).toBe('Stock Code');
      expect(trs.at(2).text().trim()).toBe('Shares');
      expect(trs.at(3).text().trim()).toBe('Per Stock');
      expect(trs.at(4).text().trim()).toBe('Fees');
      expect(trs.at(5).text().trim()).toBe('Adjusted Amount');
      expect(trs.at(6).text().trim()).toBe('Total Movement');

      const tds = wrapper.find('td');
      expect(tds).toHaveLength(14);
      expect(tds.at(0).text().trim()).toBe('2021-01-01');
      expect(tds.at(1).text().trim()).toBe('DEF');
      expect(tds.at(2).text().trim()).toBe('1');
      expect(tds.at(3).text().trim()).toBe('10.00 AED');
      expect(tds.at(4).text().trim()).toBe('1.00 AED');
      expect(tds.at(5).text().trim()).toBe('11.00 AED');
      expect(tds.at(6).text().trim()).toBe('11.00 AED');
      expect(tds.at(7).text().trim()).toBe('2020-01-02');
      expect(tds.at(8).text().trim()).toBe('ABC');
      expect(tds.at(9).text().trim()).toBe('1');
      expect(tds.at(10).text().trim()).toBe('10.00 AED');
      expect(tds.at(11).text().trim()).toBe('1.00 AED');
      expect(tds.at(12).text().trim()).toBe('11.00 AED');
      expect(tds.at(13).text().trim()).toBe('11.00 AED');
    });
  });

  it('Should show error message when buy Stocks fail', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.post(Endpoints.buyStockTransaction(defaultAccount.id), (req, res) => {
      return res.networkError('Error');
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const buyButton = wrapper.find('button').at(3);
    buyButton.simulate('click');
    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5); // get stocks
    });

    wrapper.find('input').at(0).simulate('change', {target: {value: 'DEF'}}); // Stock
    wrapper.find('input').at(1).simulate('change', {target: {value: 1}}); // Price
    wrapper.find('input').at(2).simulate('change', {target: {value: 1}}); // No Shares
    wrapper.find('input').at(3).simulate('change', {target: {value: 1}}); // brokerageFee
    wrapper.find('input').at(4).simulate('change', {target: {value: '2021-01-01'}}); // date
    const actualBuyButton = wrapper.find('button').at(8);
    actualBuyButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(6); // buy
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should show error notification when buy Stocks fail', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.post(Endpoints.buyStockTransaction(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(409), ctx.json(apiError));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const buyButton = wrapper.find('button').at(3);
    buyButton.simulate('click');
    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5); // get stocks
    });

    wrapper.find('input').at(0).simulate('change', {target: {value: 'DEF'}}); // Stock
    wrapper.find('input').at(1).simulate('change', {target: {value: 1}}); // Price
    wrapper.find('input').at(2).simulate('change', {target: {value: 1}}); // No Shares
    wrapper.find('input').at(3).simulate('change', {target: {value: 1}}); // brokerageFee
    wrapper.find('input').at(4).simulate('change', {target: {value: '2021-01-01'}}); // date
    const actualBuyButton = wrapper.find('button').at(8);
    actualBuyButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(6); // buy
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(NotificationError).text()).toContain('error message');
      expect(wrapper.update().find(NotificationError).text()).toContain('random error');
    });
  });

  it('Should display sell form when sell button is clicked', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const sellButton = wrapper.find('button').at(4);
    sellButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5);
    });

    await waitExpectWithAct(() => {
      const inputs = wrapper.update().find('input');
      // TODO: Add check of the input labels
      expect(inputs).toHaveLength(5);
      expect(inputs.at(0).prop('value')).toBe(''); // Stock
      expect(inputs.at(1).prop('value')).toBe(''); // Price
      expect(inputs.at(2).prop('value')).toBe(''); // No Shares
      expect(inputs.at(3).prop('value')).toBe(''); // Brokerage
      expect(inputs.at(4).prop('value')).toBe((new Date()).toISOString().split('T')[0]); // Date

      expect(wrapper.find('button').at(8).text()).toBe('Sell');
    });

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5);
    });
  });

  it('Should fail when get Stocks fail on sell', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res) => {
      return res.networkError('error');
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const sellButton = wrapper.find('button').at(4);
    sellButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5);
    });

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should hide sell form when sell button is clicked again', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4);
    });

    const sellButton = wrapper.find('button').at(4);
    sellButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find('input')).toHaveLength(5);
      expect(mockedServer.receivedCalls()).toBe(5);
    });

    sellButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find('input')).toHaveLength(0);
    });
  });

  it('Should update table and account summary when a transaction is sold', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    let stocksTransactionsCall = 0;
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      stocksTransactionsCall += 1;
      if (stocksTransactionsCall === 1) {
        return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
      } else {
        return res(ctx.status(200), ctx.json([stockBuyTransactionRow,
          {
            ...stockSellTransactionRow,
            date: '2021-01-01',
            stockCode: 'DEF',
          }]));
      }
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.post(Endpoints.sellStockTransaction(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json({}));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    let accountSummaryCall = 0;
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      accountSummaryCall += 1;
      if (accountSummaryCall === 1) {
        return res(ctx.status(200), ctx.json(accountSummary));
      } else {
        return res(ctx.status(200), ctx.json({
          ...accountSummary,
          lastDepositOrWithdrawalDate: '2021-01-01',
          amount: 11999,
        }));
      }
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const sellButton = wrapper.find('button').at(4);
    sellButton.simulate('click');
    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5); // get stocks
    });

    wrapper.find('input').at(0).simulate('change', {target: {value: 'DEF'}}); // Stock
    wrapper.find('input').at(1).simulate('change', {target: {value: 1}}); // Price
    wrapper.find('input').at(2).simulate('change', {target: {value: 1}}); // No Shares
    wrapper.find('input').at(3).simulate('change', {target: {value: '2021-01-01'}}); // date
    wrapper.find('input').at(4).simulate('change', {target: {value: 1}}); // brokerageFee
    const actualSellButton = wrapper.find('button').at(8);
    actualSellButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(8); // sell, update transaction and stocks

      const inputs = wrapper.update().find('input');
      expect(inputs).toHaveLength(0);

      const accountSummary = wrapper.update().first('#AccountSummary').text();
      expect(accountSummary).toContain('Buying Power');
      expect(accountSummary).toContain('2021-01-01');
      expect(accountSummary).toContain('11,999.00');
      expect(accountSummary).toContain('PHP');

      const trs = wrapper.update().find('th');
      expect(trs).toHaveLength(7);
      expect(trs.at(0).text().trim()).toBe('Date');
      expect(trs.at(1).text().trim()).toBe('Stock Code');
      expect(trs.at(2).text().trim()).toBe('Shares');
      expect(trs.at(3).text().trim()).toBe('Per Stock');
      expect(trs.at(4).text().trim()).toBe('Fees');
      expect(trs.at(5).text().trim()).toBe('Adjusted Amount');
      expect(trs.at(6).text().trim()).toBe('Total Movement');

      const tds = wrapper.update().find('td');
      expect(tds).toHaveLength(14);
      expect(tds.at(0).text().trim()).toBe('2021-01-01');
      expect(tds.at(1).text().trim()).toBe('DEF');
      expect(tds.at(2).text().trim()).toBe('1');
      expect(tds.at(3).text().trim()).toBe('10.00 AED');
      expect(tds.at(4).text().trim()).toBe('1.00 AED');
      expect(tds.at(5).text().trim()).toBe('9.00 AED');
      expect(tds.at(6).text().trim()).toBe('9.00 AED');
      expect(tds.at(7).text().trim()).toBe('2020-01-02');
      expect(tds.at(8).text().trim()).toBe('ABC');
      expect(tds.at(9).text().trim()).toBe('1');
      expect(tds.at(10).text().trim()).toBe('10.00 AED');
      expect(tds.at(11).text().trim()).toBe('1.00 AED');
      expect(tds.at(12).text().trim()).toBe('11.00 AED');
      expect(tds.at(13).text().trim()).toBe('11.00 AED');
    });
  });

  it('Should show error message when sell Stocks fail', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.post(Endpoints.sellStockTransaction(defaultAccount.id), (req, res) => {
      return res.networkError('Error');
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transaction and accounts
    });

    const sellButton = wrapper.find('button').at(4);
    sellButton.simulate('click');
    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5); // get stocks
    });

    wrapper.find('input').at(0).simulate('change', {target: {value: 'DEF'}}); // Stock
    wrapper.find('input').at(1).simulate('change', {target: {value: 1}}); // Price
    wrapper.find('input').at(2).simulate('change', {target: {value: 1}}); // No Shares
    wrapper.find('input').at(3).simulate('change', {target: {value: 1}}); // brokerageFee
    wrapper.find('input').at(4).simulate('change', {target: {value: '2021-01-01'}}); // date
    const actualSellButton = wrapper.find('button').at(8);
    actualSellButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(6); // sell
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should show error notification when sell Stocks fail', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.post(Endpoints.sellStockTransaction(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(400), ctx.json(apiError));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transaction and accounts
    });

    const sellButton = wrapper.find('button').at(4);
    sellButton.simulate('click');
    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5); // get stocks
    });

    wrapper.find('input').at(0).simulate('change', {target: {value: 'DEF'}}); // Stock
    wrapper.find('input').at(1).simulate('change', {target: {value: 1}}); // Price
    wrapper.find('input').at(2).simulate('change', {target: {value: 1}}); // No Shares
    wrapper.find('input').at(3).simulate('change', {target: {value: 1}}); // brokerageFee
    wrapper.find('input').at(4).simulate('change', {target: {value: '2021-01-01'}}); // date
    const actualSellButton = wrapper.find('button').at(8);
    actualSellButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(6); // sell
      expect(wrapper.update().find(NotificationError).text()).toContain('error message');
      expect(wrapper.update().find(NotificationError).text()).toContain('random error');
    });
  });

  it('Should display confirmation message when undo button is clicked', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const undoButton = wrapper.find('button').at(5);
    undoButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().text()).toContain('Are you sure you want to delete the last transaction you did?');
      expect(wrapper.find('button').at(8).text()).toBe('Delete');
    });
  });

  it('Should hide confirmation message when undo button is clicked again', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const undoButton = wrapper.find('button').at(5);
    undoButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().text()).toContain('Are you sure you want to delete the last transaction you did?');
    });

    undoButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().text()).not.toContain('Are you sure you want to delete the last transaction you did?');
    });
  });

  it('Should update table and account summary when a transaction is undone', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    let stocksJournalCall = 0;
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      stocksJournalCall += 1;
      if (stocksJournalCall === 1) {
        return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
      } else {
        return res(ctx.status(200), ctx.json([]));
      }
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.delete(Endpoints.lastTransaction(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json({}));
    }));
    let accountSummaryCall = 0;
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      accountSummaryCall += 1;
      if (accountSummaryCall === 1) {
        return res(ctx.status(200), ctx.json(accountSummary));
      } else {
        return res(ctx.status(200), ctx.json({
          ...accountSummary,
          lastDepositOrWithdrawalDate: '2021-01-01',
          amount: 12001,
        }));
      }
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transaction and accounts summary
    });

    const undoButton = wrapper.find('button').at(5);
    undoButton.simulate('click');

    const actualUndoButton = wrapper.find('button').at(8);
    actualUndoButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(7); // delete, updated transaction and account summary

      expect(wrapper.update().text()).not.toContain('Are you sure you want to delete the last transaction you did?');

      const accountSummary = wrapper.update().first('#AccountSummary').text();
      expect(accountSummary).toContain('Buying Power');
      expect(accountSummary).toContain('2021-01-01');
      expect(accountSummary).toContain('12,001.00');
      expect(accountSummary).toContain('PHP');

      const trs = wrapper.update().find('th');
      expect(trs).toHaveLength(7);
      expect(trs.at(0).text().trim()).toBe('Date');
      expect(trs.at(1).text().trim()).toBe('Stock Code');
      expect(trs.at(2).text().trim()).toBe('Shares');
      expect(trs.at(3).text().trim()).toBe('Per Stock');
      expect(trs.at(4).text().trim()).toBe('Fees');
      expect(trs.at(5).text().trim()).toBe('Adjusted Amount');
      expect(trs.at(6).text().trim()).toBe('Total Movement');

      const tds = wrapper.update().find('td');
      expect(tds).toHaveLength(0);
    });
  });

  it('Should show error message when undoing Stocks fail', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.delete(Endpoints.lastTransaction(defaultAccount.id), (req, res) => {
      return res(ctx.status(500), ctx.json({}));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const undoButton = wrapper.find('button').at(5);
    undoButton.simulate('click');

    const deleteButton = wrapper.find('button').at(8);
    deleteButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(5); // delete
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should display deposit form when deposit button is clicked', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const depositButton = wrapper.find('button').at(6);
    depositButton.simulate('click');

    await waitExpectWithAct(() => {
      const inputs = wrapper.update().find('input');
      // TODO: Add check of the input labels
      expect(inputs).toHaveLength(2);
      expect(inputs.at(0).prop('value')).toBe(''); // Amount
      expect(inputs.at(1).prop('value')).toBe((new Date()).toISOString().split('T')[0]); // Date

      expect(wrapper.find('button').at(8).text()).toBe('Deposit');
    });
  });

  it('Should hide deposit form when deposit button is clicked again', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const depositButton = wrapper.find('button').at(6);
    depositButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find('input')).toHaveLength(2);
    });

    depositButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find('input')).toHaveLength(0);
    });
  });

  it('Should update account summary and table  when a deposit is done', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    let accountMovementsCall = 0;
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      accountMovementsCall += 1;
      if (accountMovementsCall === 1) {
        return res(ctx.status(200), ctx.json([]));
      } else {
        return res(ctx.status(200), ctx.json([accountDepositRow]));
      }
    }));
    mockedServer.use(rest.post(Endpoints.depositInAccount(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json({}));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    let accountSummaryCall = 0;
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      accountSummaryCall += 1;
      if (accountSummaryCall === 1) {
        return res(ctx.status(200), ctx.json(accountSummary));
      } else {
        return res(ctx.status(200), ctx.json({
          ...stockBuyTransactionRow,
          amount: 12001,
          lastDepositOrWithdrawalDate: '2021-01-01',
        }));
      }
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const depositButton = wrapper.find('button').at(6);
    depositButton.simulate('click');

    wrapper.find('input').at(0).simulate('change', {target: {value: 1}}); // Amount
    wrapper.find('input').at(1).simulate('change', {target: {value: '2021-01-01'}}); // Date
    const actualDepositButton = wrapper.find('button').at(8);
    actualDepositButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(7); // deposit, update account

      const inputs = wrapper.update().find('input');
      expect(inputs).toHaveLength(0);

      const accountSummary = wrapper.update().first('#AccountSummary').text();
      expect(accountSummary).toContain('Buying Power');
      expect(accountSummary).toContain('2021-01-01');
      expect(accountSummary).toContain('12,001.00');
      expect(accountSummary).toContain('AED');

      const trs = wrapper.find('th');
      expect(trs).toHaveLength(7);
      expect(trs.at(0).text().trim()).toBe('Date');
      expect(trs.at(1).text().trim()).toBe('Stock Code');
      expect(trs.at(2).text().trim()).toBe('Shares');
      expect(trs.at(3).text().trim()).toBe('Per Stock');
      expect(trs.at(4).text().trim()).toBe('Fees');
      expect(trs.at(5).text().trim()).toBe('Adjusted Amount');
      expect(trs.at(6).text().trim()).toBe('Total Movement');

      const tds = wrapper.find('td');
      expect(tds).toHaveLength(7);
      expect(tds.at(0).text().trim()).toBe('2020-01-01');
      expect(tds.at(1).text().trim()).toBe('--');
      expect(tds.at(2).text().trim()).toBe('--');
      expect(tds.at(3).text().trim()).toBe('--');
      expect(tds.at(4).text().trim()).toBe('--');
      expect(tds.at(5).text().trim()).toBe('--');
      expect(tds.at(6).text().trim()).toBe('100.00 AED');
    });
  });

  it('Should show error message when deposit fails', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.post(Endpoints.depositInAccount(defaultAccount.id), (req, res) => {
      return res(ctx.status(400), ctx.json({}));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const depositButton = wrapper.find('button').at(6);
    depositButton.simulate('click');

    wrapper.find('input').at(0).simulate('change', {target: {value: 1}}); // Amount
    wrapper.find('input').at(1).simulate('change', {target: {value: '2021-01-01'}}); // Date
    const actualDepositButton = wrapper.find('button').at(8);
    actualDepositButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(5); // Deposit
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should display withdraw form when withdraw button is clicked', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const withdrawButton = wrapper.find('button').at(7);
    withdrawButton.simulate('click');

    await waitExpectWithAct(() => {
      const inputs = wrapper.update().find('input');
      // TODO: Add check of the input labels
      expect(inputs).toHaveLength(2);
      expect(inputs.at(0).prop('value')).toBe(''); // Amount
      expect(inputs.at(1).prop('value')).toBe((new Date()).toISOString().split('T')[0]); // Date

      expect(wrapper.find('button').at(8).text()).toBe('Withdraw');
    });
  });

  it('Should hide withdraw form when withdraw button is clicked again', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const withdrawButton = wrapper.find('button').at(7);
    withdrawButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find('input')).toHaveLength(2);
    });

    withdrawButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find('input')).toHaveLength(0);
    });
  });

  it('Should update account summary and table when a withdraw is done', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    let accountMovementsCall = 0;
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      accountMovementsCall += 1;
      if (accountMovementsCall === 1) {
        return res(ctx.status(200), ctx.json([]));
      } else {
        return res(ctx.status(200), ctx.json([accountWithdrawRow]));
      }
    }));
    mockedServer.use(rest.post(Endpoints.withdrawFromAccount(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json({}));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    let accountSummaryCall = 0;
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      accountSummaryCall += 1;
      if (accountSummaryCall === 1) {
        return res(ctx.status(200), ctx.json(accountSummary));
      } else {
        return res(ctx.status(200), ctx.json({
          ...stockBuyTransactionRow,
          amount: 11999,
          lastDepositOrWithdrawalDate: '2021-01-01',
        }));
      }
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const withdrawButton = wrapper.find('button').at(7);
    withdrawButton.simulate('click');

    wrapper.find('input').at(0).simulate('change', {target: {value: 1}}); // Amount
    wrapper.find('input').at(1).simulate('change', {target: {value: '2021-01-01'}}); // Date
    const actualWithdrawButton = wrapper.find('button').at(8);
    actualWithdrawButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(7); // withdraw and accounts

      const inputs = wrapper.update().find('input');
      expect(inputs).toHaveLength(0);

      const accountSummary = wrapper.update().first('#AccountSummary').text();
      expect(accountSummary).toContain('Buying Power');
      expect(accountSummary).toContain('2021-01-01');
      expect(accountSummary).toContain('11,999.00');
      expect(accountSummary).toContain('AED');

      const trs = wrapper.find('th');
      expect(trs).toHaveLength(7);
      expect(trs.at(0).text().trim()).toBe('Date');
      expect(trs.at(1).text().trim()).toBe('Stock Code');
      expect(trs.at(2).text().trim()).toBe('Shares');
      expect(trs.at(3).text().trim()).toBe('Per Stock');
      expect(trs.at(4).text().trim()).toBe('Fees');
      expect(trs.at(5).text().trim()).toBe('Adjusted Amount');
      expect(trs.at(6).text().trim()).toBe('Total Movement');

      const tds = wrapper.find('td');
      expect(tds).toHaveLength(7);
      expect(tds.at(0).text().trim()).toBe('2020-01-04');
      expect(tds.at(1).text().trim()).toBe('--');
      expect(tds.at(2).text().trim()).toBe('--');
      expect(tds.at(3).text().trim()).toBe('--');
      expect(tds.at(4).text().trim()).toBe('--');
      expect(tds.at(5).text().trim()).toBe('--');
      expect(tds.at(6).text().trim()).toBe('100.00 AED');
    });
  });

  it('Should show error message when withdraw fails', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.post(Endpoints.withdrawFromAccount(defaultAccount.id), (req, res) => {
      return res.networkError('Error');
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const withdrawButton = wrapper.find('button').at(7);
    withdrawButton.simulate('click');

    wrapper.find('input').at(0).simulate('change', {target: {value: 1}}); // Amount
    wrapper.find('input').at(1).simulate('change', {target: {value: '2021-01-01'}}); // Date
    const actualDepositButton = wrapper.find('button').at(8);
    actualDepositButton.simulate('click');

    await waitExpectWithAct(() => {
      wrapper.update();
      expect(mockedServer.receivedCalls()).toBe(5); // Withdraw
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should synchronize buy form when change from sell to buy actions', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const buyButton = wrapper.find('button').at(3);
    buyButton.simulate('click');

    wrapper.find('input').at(0).simulate('change', {target: {value: 'DEF'}}); // Stock
    wrapper.find('input').at(1).simulate('change', {target: {value: 1}}); // Price
    wrapper.find('input').at(2).simulate('change', {target: {value: 2}}); // No Shares
    wrapper.find('input').at(3).simulate('change', {target: {value: 3}}); // Brokerage fee
    wrapper.find('input').at(4).simulate('change', {target: {value: '2021-01-01'}}); // Date

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(5); // call stock
    });

    const sellButton = wrapper.find('button').at(4);
    sellButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(6); // call stock
      const inputs = wrapper.update().find('input');
      expect(inputs).toHaveLength(5);
      expect(inputs.at(0).prop('value')).toBe('DEF'); // Stock
      expect(inputs.at(1).prop('value')).toBe(1); // Price
      expect(inputs.at(2).prop('value')).toBe(2); // No Shares
      expect(inputs.at(3).prop('value')).toBe(3); // Brokerage fee
      expect(inputs.at(4).prop('value')).toBe('2021-01-01'); // Date

      expect(wrapper.find('button').at(8).text()).toBe('Sell');
    });
  });

  it('Should synchronize deposit form when change from deposit to withdraw actions', async () => {
    mockedServer.use(rest.get(Endpoints.defaultAccount(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(defaultAccount));
    }));
    mockedServer.use(rest.get(Endpoints.stockTransactions(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([stockBuyTransactionRow]));
    }));
    mockedServer.use(rest.get(Endpoints.accountMovements([Actions.DEPOSIT, Actions.WITHDRAW], defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));
    mockedServer.use(rest.get(Endpoints.stockCodes(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(['Stock1, Stock2']));
    }));
    mockedServer.use(rest.get(Endpoints.accountSummary(defaultAccount.id), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accountSummary));
    }));

    const wrapper = mount(<StockTransactionsPage/>);
    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(mockedServer.receivedCalls()).toBe(4); // get transactions, movements and account summary
    });

    const depositButton = wrapper.find('button').at(6);
    depositButton.simulate('click');

    wrapper.find('input').at(0).simulate('change', {target: {value: 1}}); // Amount
    wrapper.find('input').at(1).simulate('change', {target: {value: '2021-01-01'}}); // Date

    const withdrawButton = wrapper.find('button').at(7);
    withdrawButton.simulate('click');

    await waitExpectWithAct(() => {
      const inputs = wrapper.update().find('input');
      expect(inputs).toHaveLength(2);
      expect(inputs.at(0).prop('value')).toBe(1); // Amount
      expect(inputs.at(1).prop('value')).toBe('2021-01-01'); // Date

      expect(wrapper.find('button').at(8).text()).toBe('Withdraw');
    });
  });
});
