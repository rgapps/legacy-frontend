import React from 'react';
import {shallow} from 'enzyme';
import Logo from '../Logo';
import '../../../__tests/setupTests';

describe('<Logo/>', () => {
  it('renders with name and mote', () => {
    const wrapper = shallow(<Logo name={'name'} mote={'mote'}/>);

    expect(wrapper.text()).toContain('name');
    expect(wrapper.text()).toContain('mote');
  });
});
