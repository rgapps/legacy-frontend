import React from 'react';
import {shallow} from 'enzyme';
import PersonalCard from '../PersonalCard';
import Flippable from '../../Animations/Flippable';
import FrontSide from '../FrontSide';
import BackSide from '../BackSide';
import '../../../__tests/setupTests';
import {expectToThrow} from '../../../__tests/setupTests';
import each from 'jest-each';

describe('<PersonalCard />', () => {
  const personalData = {
    name: 'Wilson René Guevara Arévalo',
    mote: 'wilsonr990',
    profession: 'Software Developer',
  };

  it('Should fail if no data is given', () => {
    expectToThrow(() => shallow(<PersonalCard/>),
      'Warning: Failed prop type: The prop `data` is marked as required in `PersonalCard`, but its value is `undefined`.');
  });

  it('Should fail if data is incorrect type', () => {
    expectToThrow(() => shallow(<PersonalCard data={'dasd'}/>),
      'Warning: Failed prop type: Invalid prop `data` of type `string` supplied to `PersonalCard`, expected `object`.');
  });

  each([
    [{other: 'other'}, 'data.name'],
    [{name: 'name', other: 'other'}, 'data.mote'],
    [{name: 'name', mote: 'mote', other: 'other'}, 'data.profession'],
  ]).it('Should fail if data does not match shape', (data, field) => {
    expectToThrow(() => shallow(<PersonalCard data={data}/>),
      `Warning: Failed prop type: The prop \`${field}\` is marked as required in \`PersonalCard\`, but its value is \`undefined\`.`);
  });

  it('Should be flippable', () => {
    const personalData = {
      name: 'Wilson René Guevara Arévalo',
      mote: 'wilsonr990',
      profession: 'Software Developer',
    };

    const wrapper = shallow(<PersonalCard data={personalData}/>);
    const flippable = wrapper.find(Flippable);
    expect(flippable.exists()).toBeTruthy();
  });

  it('Should have front and back sides', () => {
    const wrapper = shallow(<PersonalCard data={personalData}/>);

    const flippable = wrapper.find(Flippable);
    expect(flippable.exists()).toBeTruthy();
    const flippableSides = flippable.children();
    expect(flippableSides.length).toBe(2);
    expect(flippableSides.at(0).find(FrontSide).exists()).toBeTruthy();
    expect(flippableSides.at(0).find(FrontSide).prop('data')).toBe(personalData);
    expect(flippableSides.at(1).find(BackSide).exists()).toBeTruthy();
    expect(flippableSides.at(1).find(BackSide).prop('text')).not.toBeTruthy();
  });
});
