import React from 'react';
import {mount} from 'enzyme';
import '../../../__tests/setupTests';
import LockingError from '../LockingError';
import {waitExpectWithAct} from '../../../__tests/setupTests';

describe('<LockingError/>', () => {
  it('renders with error', () => {
    const wrapper = mount(<LockingError error={'Error Message'}/>);

    expect(wrapper.text()).toContain('Network Error');
    expect(wrapper.text()).toContain('Error Message');
    expect(wrapper.find('button').text()).toBe('Refresh');
  });

  it('renders with error and message', () => {
    const wrapper = mount(<LockingError error={'Error'} message={'error message'}/>);

    expect(wrapper.text()).toContain('Network Error');
    expect(wrapper.text()).toContain('Error');
    expect(wrapper.text()).toContain('error message');
    expect(wrapper.find('button').text()).toBe('Refresh');
  });

  it('reload page if refresh clicked', async () => {
    const wrapper = mount(<LockingError error={'Error Message'}/>);

    expect(wrapper.text()).toContain('Error Message');
    expect(wrapper.find('button').text()).toBe('Refresh');

    wrapper.find('button').simulate('click');

    await waitExpectWithAct(() => {
      expect(window.location.reload).toBeCalled();
    });
  });
});
