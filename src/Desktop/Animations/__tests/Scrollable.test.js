import React from 'react';
import Scrollable from '../Scrollable';
import {mount, shallow} from 'enzyme';
import {AnimationBehaviorContext} from '../Behavior/AnimationBehaviorHandler';
import {AnimationType, defaultAnimationBehavior, RunningAnimation} from '../Behavior/behaviorDefinitions';
import {expectToThrow, sleep, waitExpectWithAct} from '../../../__tests/setupTests';

describe('<Scrollable />', () => {
  it('Fail when no content width is provided', () => {
    expectToThrow(() => shallow(<Scrollable/>),
      'Warning: Failed prop type: The prop `contentWidth` is marked as required in `Scrollable`, but its value is `undefined`');
  });

  it('Fail when no content height is provided', () => {
    expectToThrow(() => shallow(<Scrollable contentWidth={100}/>),
      'Warning: Failed prop type: The prop `contentHeight` is marked as required in `Scrollable`, but its value is `undefined`');
  });

  it('fail when more than one children are provided', () => {
    expectToThrow(() => shallow(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>1</div>
        <div>2</div>
      </Scrollable>,
    ), 'Warning: Failed prop type: `Scrollable` should contain one children (2 received)');
  });

  it('Fail when no children are provided', () => {
    expectToThrow(() => shallow(<Scrollable contentWidth={100} contentHeight={100}/>),
      'Warning: Failed prop type: `Scrollable` should contain one children (0 received)');
  });

  it('Contains Right, Left, Top and Bottom buttons. And also children content', () => {
    const wrapper = shallow(
      <Scrollable contentWidth={100} contentHeight={100} id={'test'}>
        <div>front</div>
      </Scrollable>,
    );

    expect(wrapper.find('[data-testid="top"]').length).toBe(1);
    expect(wrapper.find('[data-testid="left"]').length).toBe(1);
    expect(wrapper.find('[data-testid="right"]').length).toBe(1);
    expect(wrapper.find('[data-testid="left"]').length).toBe(1);
    expect(wrapper.find('[data-testid="scrollable"]').length).toBe(1);
    expect(wrapper.find('[data-testid="scrollable"]').childAt(0).html()).toBe('<div id="test"><div>front</div></div>');
  });

  it('Renders centered by default', () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div style={{width: '100px', height: '100px'}}>front</div>
      </Scrollable>,
    );

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
      .toBe('none');
  });

  it('Scrolls when clicked top border', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="top"]').simulate('mouseEnter');
    wrapper.find('div[data-testid="top"]').simulate('mouseDown');
    wrapper.find('div[data-testid="top"]').simulate('mouseUp');
    wrapper.find('div[data-testid="top"]').simulate('mouseLeave');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(10px) translateZ(0)');
    });
  });

  it('Scrolls when clicked bottom border', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="bottom"]').simulate('mouseEnter');
    wrapper.find('div[data-testid="bottom"]').simulate('mouseDown');
    wrapper.find('div[data-testid="bottom"]').simulate('mouseUp');
    wrapper.find('div[data-testid="bottom"]').simulate('mouseLeave');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(-10px) translateZ(0)');
    });
  });

  it('Scrolls when clicked right border', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="right"]').simulate('mouseEnter');
    wrapper.find('div[data-testid="right"]').simulate('mouseDown');
    wrapper.find('div[data-testid="right"]').simulate('mouseUp');
    wrapper.find('div[data-testid="right"]').simulate('mouseLeave');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(-10px) translateY(0px) translateZ(0)');
    });
  });

  it('Scrolls when clicked left border', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="left"]').simulate('mouseEnter');
    wrapper.find('div[data-testid="left"]').simulate('mouseDown');
    wrapper.find('div[data-testid="left"]').simulate('mouseUp');
    wrapper.find('div[data-testid="left"]').simulate('mouseLeave');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(10px) translateY(0px) translateZ(0)');
    });
  });

  it('Scrolls with wheel', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div').first().simulate('wheel', {deltaX: 1, deltaY: 1});
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(-14px) translateY(-14px) translateZ(0)');
    });
  });

  it('Scrolls with touch', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div').first().simulate('touchstart', {touches: [{clientX: 0, clientY: 0}]});
    wrapper.find('div').first().simulate('touchmove', {touches: [{clientX: 1, clientY: 1}]});
    wrapper.find('div').first().simulate('touchend', {touches: []});
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(14px) translateY(14px) translateZ(0)');
    });
  });

  it('Dont scroll with touch if more than one touch point', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div').first().simulate('touchstart', {touches: [{clientX: 0, clientY: 0}, {clientX: 1, clientY: 2}]});
    wrapper.find('div').first().simulate('touchmove', {touches: [{clientX: 1, clientY: 1}, {clientX: 2, clientY: 2}]});
    wrapper.find('div').first().simulate('touchend', {touches: []});
    await sleep(100);
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    });
  });

  it('Keep scrolling until border', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="top"]').simulate('mouseEnter');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(40px) translateZ(0)');
    });
  });

  it('Scrolling from border to border X axis', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="right"]').simulate('mouseEnter');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(-40px) translateY(0px) translateZ(0)');
    });
    wrapper.find('div[data-testid="right"]').simulate('mouseLeave');
    wrapper.find('div[data-testid="left"]').simulate('mouseEnter');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(40px) translateY(0px) translateZ(0)');
    });
  });

  it('Scrolling from border to border Y axis', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="top"]').simulate('mouseEnter');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(40px) translateZ(0)');
    });
    wrapper.find('div[data-testid="top"]').simulate('mouseLeave');
    wrapper.find('div[data-testid="bottom"]').simulate('mouseEnter');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(-40px) translateZ(0)');
    });
  });

  it('Dont scroll if content is visible in the screen', async () => {
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 200});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 200});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
      .toBe('none');
    wrapper.find('div[data-testid="top"]').simulate('mouseEnter');
    await sleep(100);
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    });
  });

  it('Call callbacks when scrolled', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();
    const wrapper = mount(
      <Scrollable contentWidth={100} contentHeight={100} onScrollStart={callbackStart} onScrolled={callbackEnd}>
        <div>front</div>
      </Scrollable>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="top"]').simulate('mouseEnter');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(40px) translateZ(0)');
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(0);
    });
    wrapper.find('div[data-testid="top"]').simulate('mouseLeave');
    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
    });
  });

  it('Call AnimationBehaviorContext start and stop when scrolled with border', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider
        value={animationBehavior}>
        <Scrollable contentWidth={100} contentHeight={100} id={'Scrollable-test'}>
          <div>front</div>
        </Scrollable>
      </AnimationBehaviorContext.Provider>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="top"]').simulate('mouseEnter');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(40px) translateZ(0)');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.start).toBeCalledTimes(1);
      expect(animationBehavior.stop).toBeCalledTimes(0);
    });
    wrapper.find('div[data-testid="top"]').simulate('mouseLeave');
    wrapper.find('div[data-testid="bottom"]').simulate('mouseEnter');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(-40px) translateZ(0)');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.start).toBeCalledTimes(2);
      expect(animationBehavior.stop).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.stop).toBeCalledTimes(1);
    });
    wrapper.find('div[data-testid="bottom"]').simulate('mouseLeave');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(-40px) translateZ(0)');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.start).toBeCalledTimes(2);
      expect(animationBehavior.stop).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.stop).toBeCalledTimes(2);
    });
  });

  it('Call AnimationBehaviorContext start and stop when scrolled with wheel until border', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider
        value={animationBehavior}>
        <Scrollable contentWidth={100} contentHeight={100} id={'Scrollable-test'}>
          <div>front</div>
        </Scrollable>
      </AnimationBehaviorContext.Provider>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div').first().simulate('wheel', {deltaX: 0, deltaY: -1});
    expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
    expect(animationBehavior.start).toBeCalledTimes(1);
    expect(animationBehavior.stop).toBeCalledTimes(0);
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(20px) translateZ(0)');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.start).toBeCalledTimes(1);
      expect(animationBehavior.stop).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.stop).toBeCalledTimes(1);
    });
    wrapper.find('div').first().simulate('wheel', {deltaX: 0, deltaY: -1});
    expect(animationBehavior.start).toBeCalledTimes(2);
    expect(animationBehavior.stop).toBeCalledTimes(1);
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(40px) translateZ(0)');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.start).toBeCalledTimes(2);
      expect(animationBehavior.stop).toBeCalledTimes(2);
    });
    wrapper.find('div').first().simulate('wheel', {deltaX: 0, deltaY: 1});
    expect(animationBehavior.start).toBeCalledTimes(3);
    expect(animationBehavior.stop).toBeCalledTimes(2);
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(20px) translateZ(0)');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.start).toBeCalledTimes(3);
      expect(animationBehavior.stop).toBeCalledTimes(3);
    });
  });

  it('Dont scroll if already scrolling', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider
        value={animationBehavior}>
        <Scrollable contentWidth={100} contentHeight={100} id={'Scrollable-test'}>
          <div>front</div>
        </Scrollable>
      </AnimationBehaviorContext.Provider>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="top"]').simulate('mouseEnter');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(40px) translateZ(0)');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.start).toBeCalledTimes(1);
      expect(animationBehavior.stop).toBeCalledTimes(0);
    });
    wrapper.find('div[data-testid="bottom"]').simulate('mouseEnter');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(40px) translateZ(0)');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.start).toBeCalledTimes(1);
      expect(animationBehavior.stop).toBeCalledTimes(0);
    });
    wrapper.find('div[data-testid="top"]').simulate('mouseLeave');
    await waitExpectWithAct(() => {
      expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform'))
        .toBe('translateX(0px) translateY(40px) translateZ(0)');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.start).toBeCalledTimes(1);
      expect(animationBehavior.stop).toBeCalledWith(new RunningAnimation(AnimationType.Scroll, 'Scrollable-test'));
      expect(animationBehavior.stop).toBeCalledTimes(1);
    });
  });

  it('Dont scroll if disabled by AnimationBehaviorContext', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      allowed: () => false,
      start: jest.fn(),
      stop: jest.fn(),
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <Scrollable contentWidth={100} contentHeight={100} id={'Scrollable-test'}>
          <div>front</div>
        </Scrollable>
      </AnimationBehaviorContext.Provider>,
    );
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 20});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 20});

    expect(wrapper.find('div[data-testid="scrollable"]').render().css('transform')).toBe('none');
    wrapper.find('div[data-testid="top"]').simulate('mouseEnter');
    wrapper.find('div[data-testid="top"]').simulate('mouseLeave');
    expect(animationBehavior.start).toBeCalledTimes(0);
    expect(animationBehavior.stop).toBeCalledTimes(0);
    await sleep(100);
    await waitExpectWithAct(() => {
      expect(animationBehavior.start).toBeCalledTimes(0);
      expect(animationBehavior.stop).toBeCalledTimes(0);
    });
  });
});
