import React from 'react';
import {shallow} from 'enzyme';
import '../../../__tests/setupTests';
import DeskPage from '../DeskPage';
import Desk from '../Desk';

it('renders without crashing', () => {
  const wrapper = shallow(<DeskPage/>);

  const desk = wrapper.find(Desk);
  expect(desk.exists()).toBeTruthy();
});
