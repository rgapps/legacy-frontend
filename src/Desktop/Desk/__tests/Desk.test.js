import React from 'react';
import {shallow} from 'enzyme';
import Draggable from '../../Animations/Draggable';
import PersonalCard from '../../PersonalCard/PersonalCard';
import Notebook from '../../Notebook/Notebook';
import Desk from '../Desk';
import '../../../__tests/setupTests';
import {act} from '@testing-library/react';
import PaperForm from '../../PaperForm/PaperForm';
import Chronometer from '../../Chronometer/Chronometer';
import Calculator from '../../Calculator/Calculator';
import LanguageCardsGame from '../../LanguageCards/LanguageCardsGame';
import Chess from '../../Chess/Chess';

describe('<Desk/>', () => {
  it('Parses elements as Draggables', () => {
    const wrapper = shallow(
      <Desk/>,
    );

    const draggables = wrapper.find(Draggable);
    expect(draggables.length).toBe(8);
    expect(draggables.at(0).find(PersonalCard)).toBeTruthy();
    expect(draggables.at(1).find(Notebook)).toBeTruthy();
    expect(draggables.at(2).find(PaperForm)).toBeTruthy();
    expect(draggables.at(3).find(Chronometer)).toBeTruthy();
    expect(draggables.at(4).find(LanguageCardsGame)).toBeTruthy();
    expect(draggables.at(5).find(LanguageCardsGame)).toBeTruthy();
    expect(draggables.at(6).find(Calculator)).toBeTruthy();
    expect(draggables.at(7).find(Chess)).toBeTruthy();
  });

  it('on dragged the element its z position is top', async () => {
    const wrapper = shallow(
      <Desk/>,
    );

    expect(wrapper.find(Draggable).length).toBe(8);
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(4).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(5).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(6).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(7).prop('z')).toBe(0);
    await act(async () => wrapper.find(Draggable).at(0).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(4).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(5).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(6).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(7).prop('z')).toBe(0);
    await act(async () => wrapper.find(Draggable).at(1).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(4).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(5).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(6).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(7).prop('z')).toBe(0);
    await act(async () => wrapper.find(Draggable).at(2).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(4).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(5).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(6).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(7).prop('z')).toBe(0);
    await act(async () => wrapper.find(Draggable).at(3).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(4).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(5).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(6).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(7).prop('z')).toBe(0);
    await act(async () => wrapper.find(Draggable).at(4).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(4).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(5).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(6).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(7).prop('z')).toBe(0);
    await act(async () => wrapper.find(Draggable).at(5).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(4).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(5).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(6).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(7).prop('z')).toBe(0);
    await act(async () => wrapper.find(Draggable).at(6).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(4).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(5).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(6).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(7).prop('z')).toBe(0);
    await act(async () => wrapper.find(Draggable).at(7).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(4).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(5).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(6).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(7).prop('z')).toBe(1);
  });

  it('display horizontally if is horizontal screen', async () => {
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 200});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 100});

    const wrapper = shallow(
      <Desk/>,
    );

    expect(wrapper.find(Draggable).length).toBe(8);
    expect(wrapper.find(Draggable).at(0).prop('x')).toBe(40);
    expect(wrapper.find(Draggable).at(0).prop('y')).toBe(44);
    expect(wrapper.find(Draggable).at(1).prop('x')).toBe(60);
    expect(wrapper.find(Draggable).at(1).prop('y')).toBe(50);
    expect(wrapper.find(Draggable).at(2).prop('x')).toBe(60);
    expect(wrapper.find(Draggable).at(2).prop('y')).toBe(72);
    expect(wrapper.find(Draggable).at(3).prop('x')).toBe(60);
    expect(wrapper.find(Draggable).at(3).prop('y')).toBe(25);
    expect(wrapper.find(Draggable).at(4).prop('x')).toBe(40);
    expect(wrapper.find(Draggable).at(4).prop('y')).toBe(25);
    expect(wrapper.find(Draggable).at(5).prop('x')).toBe(50);
    expect(wrapper.find(Draggable).at(5).prop('y')).toBe(10);
    expect(wrapper.find(Draggable).at(6).prop('x')).toBe(40);
    expect(wrapper.find(Draggable).at(6).prop('y')).toBe(66);
    expect(wrapper.find(Draggable).at(7).prop('x')).toBe(15);
    expect(wrapper.find(Draggable).at(7).prop('y')).toBe(44);
  });

  it('display vertically if is vertical screen', async () => {
    Object.defineProperty(document.documentElement, 'clientWidth', {writable: true, configurable: true, value: 100});
    Object.defineProperty(document.documentElement, 'clientHeight', {writable: true, configurable: true, value: 200});

    const wrapper = shallow(
      <Desk/>,
    );

    expect(wrapper.find(Draggable).length).toBe(8);
    expect(wrapper.find(Draggable).at(0).prop('x')).toBe(50);
    expect(wrapper.find(Draggable).at(0).prop('y')).toBe(30);
    expect(wrapper.find(Draggable).at(1).prop('x')).toBe(50);
    expect(wrapper.find(Draggable).at(1).prop('y')).toBe(60);
    expect(wrapper.find(Draggable).at(2).prop('x')).toBe(50);
    expect(wrapper.find(Draggable).at(2).prop('y')).toBe(90);
    expect(wrapper.find(Draggable).at(3).prop('x')).toBe(50);
    expect(wrapper.find(Draggable).at(3).prop('y')).toBe(20);
    expect(wrapper.find(Draggable).at(4).prop('x')).toBe(25);
    expect(wrapper.find(Draggable).at(4).prop('y')).toBe(50);
    expect(wrapper.find(Draggable).at(5).prop('x')).toBe(25);
    expect(wrapper.find(Draggable).at(5).prop('y')).toBe(50);
    expect(wrapper.find(Draggable).at(6).prop('x')).toBe(50);
    expect(wrapper.find(Draggable).at(6).prop('y')).toBe(72);
    expect(wrapper.find(Draggable).at(7).prop('x')).toBe(50);
    expect(wrapper.find(Draggable).at(7).prop('y')).toBe(30);
  });
});
