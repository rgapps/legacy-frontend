import React from 'react';
import {shallow} from 'enzyme';
import FrontSide from '../FrontSide';
import Logo from '../Logo';
import '../../../__tests/setupTests';

describe('<FrontSide/>', () => {
  it('renders with shared data', () => {
    const personalData = {
      name: 'Wilson René Guevara Arévalo',
      mote: 'wilsonr990',
      profession: 'Software Developer',
    };

    const wrapper = shallow(<FrontSide data={personalData}/>);
    const logo = wrapper.find(Logo).render();
    expect(logo.text()).toContain(personalData.name);
    expect(logo.text()).toContain(personalData.mote);
    expect(wrapper.text()).toContain(personalData.profession);
  });
});
