// mock window location to test window.location.assign calls
delete window.alert;
window.alert = jest.fn();

beforeEach(() => {
  window.alert.mockClear();
});
