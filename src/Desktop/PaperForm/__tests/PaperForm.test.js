import React from 'react';
import {mount, shallow} from 'enzyme';
import PaperForm from '../PaperForm';
import '../../../__tests/setupTests';
import 'jest-styled-components';
import {expectToThrow, waitExpectWithAct} from '../../../__tests/setupTests';
import each from 'jest-each';
import {defaultAnimationBehavior} from '../../../Desktop/Animations/Behavior/behaviorDefinitions';
import {AnimationBehaviorContext} from '../../../Desktop/Animations/Behavior/AnimationBehaviorHandler';

describe('<PaperForm/>', () => {
  const formInfo = {
    name: 'dummy form',
    fields: [
      {name: 'field1', text: 'field 1'},
      {name: 'field2', text: 'field 2'},
    ],
  };

  it('should fail if no content', () => {
    expectToThrow(() => shallow(<PaperForm/>),
      'Failed prop type: The prop `content` is marked as required in `PaperForm`, but its value is `undefined`.');
  });

  it('Should fail if fields is empty', () => {
    const badInfo = {
      name: 'dummy',
      fields: [],
    };
    expectToThrow(() => shallow(<PaperForm content={badInfo}/>),
      'Warning: Failed prop type: The prop \`content.fieldsLength\` should have min size 1 in `PaperForm`. Size: 0');
  });

  each([
    [{other: 'other'}, 'content.name'],
    [{name: 'name', other: 'other'}, 'content.fields'],
    [{name: 'name', fields: [{other: 'other'}], other: 'other'}, 'content.fields[0].name'],
    [{name: 'name', fields: [{name: 'name1'}], other: 'other'}, 'content.fields[0].text'],
    [{
      name: 'name',
      fields: [{name: 'name2', text: 'text'}, {other: 'other'}],
      other: 'other',
    }, 'content.fields[1].name'],
  ]).it('Should fail if content does not match shape', (content, field) => {
    expectToThrow(() => shallow(<PaperForm content={content}/>),
      `Warning: Failed prop type: The prop \`${field}\` is marked as required in \`PaperForm\`, but its value is \`undefined\`.`);
  });

  it('renders the name and the fields', () => {
    const wrapper = mount(<PaperForm content={formInfo}/>);

    expect(wrapper.text()).toContain('dummy form');
    expect(wrapper.text()).toContain('field 1');
    expect(wrapper.text()).toContain('field 2');
  });

  it('dont render inputs if it is not interactive (default)', () => {
    const wrapper = mount(<PaperForm content={formInfo}/>);

    expect(wrapper.find('input').length).toEqual(0);
  });

  it('renders inputs with empty values if it is interactive', () => {
    const wrapper = mount(<PaperForm content={formInfo} interactive={true}/>);

    expect(wrapper.find('input').length).toEqual(2);
    expect(wrapper.find('input').first().prop('value')).toEqual('');
    expect(wrapper.find('input').at(1).prop('value')).toEqual('');
  });

  it('renders inputs with values if it is interactive and currentValues is sent', () => {
    const wrapper = mount(<PaperForm
      content={formInfo} interactive={true}
      currentValues={{field1: 'value1', field2: 'value2'}}/>);

    expect(wrapper.find('input').length).toEqual(2);
    expect(wrapper.find('input').first().prop('value')).toEqual('value1');
    expect(wrapper.find('input').at(1).prop('value')).toEqual('value2');
  });

  it('click in form without interaction redirects to paperForm page', async () => {
    const wrapper = mount(<PaperForm content={formInfo}/>);

    wrapper.childAt(0).simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith('/paperForm/dummy form');
    });
  });

  it('click in form with interaction doesnt redirect', async () => {
    const wrapper = mount(<PaperForm content={formInfo} interactive={true}/>);

    wrapper.childAt(0).simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).not.toBeCalled();
    });
  });

  it('when an input is changed onChanged method is called', async () => {
    const callbackOnChange = jest.fn();
    const wrapper = mount(<PaperForm content={formInfo} interactive={true} setField={callbackOnChange}/>);

    wrapper.find('input').first().simulate('change', {target: {value: 'Hello'}});

    await waitExpectWithAct(() => {
      expect(callbackOnChange).toBeCalledTimes(1);
      expect(callbackOnChange).toBeCalledWith('field1', 'Hello');
    });
  });

  it('when an input is focused animation are disabled', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      enable: jest.fn(),
    };
    const wrapper = mount(<AnimationBehaviorContext.Provider
      value={animationBehavior}>
      <PaperForm content={formInfo} interactive={true}/>
    </AnimationBehaviorContext.Provider>);

    wrapper.find('input').first().simulate('focus');

    await waitExpectWithAct(() => {
      expect(animationBehavior.enable).toBeCalledTimes(1);
      expect(animationBehavior.enable).toBeCalledWith(false);
    });
  });

  it('when an input is blurred animation are back enabled', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      enable: jest.fn(),
    };
    const wrapper = mount(<AnimationBehaviorContext.Provider value={animationBehavior}>
      <PaperForm content={formInfo} interactive={true}/>
    </AnimationBehaviorContext.Provider>);

    wrapper.find('input').first().simulate('blur', {target: {value: 'Hello'}});

    await waitExpectWithAct(() => {
      expect(animationBehavior.enable).toBeCalledTimes(1);
      expect(animationBehavior.enable).toBeCalledWith(true);
    });
  });

  it('an input can be changed', async () => {
    // TODO: test added to verify onChange() default definition. Delete it instead when save options are implemented and tested
    const wrapper = mount(<PaperForm content={formInfo} interactive={true}/>);

    wrapper.find('input').first().simulate('change', {target: {value: 'Hello'}});
  });
});
