import React, {useState} from 'react';
import BusinessCardView from './BusinessCardView';
import {pageViewGA} from '../Common/Utils/analytics';
import {Container, Grid} from '@material-ui/core';
import {ColoredButton} from '../Common/Components/styledComponents';
import {lightGreen} from '@material-ui/core/colors';
import useRetrieve from '../Common/Services/useRetrieve';
import {Endpoints} from './../endpoints';
import styled from 'styled-components';
import ComponentSelector from './ComponentSelector';
import ErrorHandler from '../Common/Components/ErrorHandler';
import Progress from './../Common/Components/Progress';

const AdminPage = () => {
  pageViewGA();

  const {
    data: componentsData,
    loading: loadingComponents,
    error: getComponentsFailed,
  } = useRetrieve(Endpoints.components());

  const [selectedComponent, onComponentSelected] = useState(null);

  return <Container>
    <h1>COMPONENTS ADMINISTRATOR</h1>
    <Wrapper>
      <Grid container spacing={0}>
        <Grid item xs={12} md={3} style={{maxHeight: '65vh'}}>
          <ComponentSelector
            onComponentSelected={onComponentSelected}
            componentsData={componentsData}/>
        </Grid>
        <Grid item xs={12} md={9}>
          <ComponentView>
            {selectedComponent ?
              <BusinessCardView id={selectedComponent}/> :
              <div className={'welcomeMessage'}>Welcome, Select an item to edit it</div>}
          </ComponentView>
        </Grid>
        <CreateComponentButton colored={lightGreen}>
          CREATE COMPONENT
        </CreateComponentButton>
      </Grid>
    </Wrapper>
    {loadingComponents && <Progress/>}
    {getComponentsFailed && <ErrorHandler error={getComponentsFailed}/>}
  </Container>;
};

const Wrapper = styled.div`
  border: 1px lightgray solid;
  border-radius: 1px;
`;

const ComponentView = styled.div`
  min-height: 65vh;
  display: flex;
  justify-content: center;

  .welcomeMessage {
    color: gray;
    font-size: 15px;
    margin: auto;
  }
`;

const CreateComponentButton = styled(ColoredButton)`
  width: 100%;
  border-radius: 0;
`;

export default AdminPage;
