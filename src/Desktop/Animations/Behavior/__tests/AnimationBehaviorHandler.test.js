import React from 'react';
import {mount, shallow} from 'enzyme';
import '../../../../__tests/setupTests';
import AnimationBehaviorHandler, {AnimationBehaviorContext} from '../AnimationBehaviorHandler';
import {
  AnimationsBehaviorType,
  AnimationType,
  defaultAnimationBehavior,
  RunningAnimation,
} from '../behaviorDefinitions';
import {expectToThrow} from '../../../../__tests/setupTests';

describe('<AnimationBehaviorHandler />', () => {
  const TestAnimationBehaviorContextConsumer = <AnimationBehaviorContext.Consumer>{(context) => (
    <div>
      <div id={'dragAllowed'}>{context.allowed(AnimationType.Drag) ? 'true' : 'false'}</div>
      <div id={'flipAllowed'}>{context.allowed(AnimationType.Flip) ? 'true' : 'false'}</div>
      <div id={'pagingAllowed'}>{context.allowed(AnimationType.Paging) ? 'true' : 'false'}</div>
      <div id={'dragRunning'}>{context.runningAny(AnimationType.Drag) ? 'true' : 'false'}</div>
      <div id={'flipRunning'}>{context.runningAny(AnimationType.Flip) ? 'true' : 'false'}</div>
      <div id={'pagingRunning'}>{context.runningAny(AnimationType.Paging) ? 'true' : 'false'}</div>
      <div id={'startDrag'} onClick={() => context.start(new RunningAnimation(AnimationType.Drag, 'id1'))}/>
      <div id={'startFlip'} onClick={() => context.start(new RunningAnimation(AnimationType.Flip, 'id2'))}/>
      <div id={'startPaging'} onClick={() => context.start(new RunningAnimation(AnimationType.Paging, 'id3'))}/>
      <div id={'stopDrag'} onClick={() => context.stop(new RunningAnimation(AnimationType.Drag, 'id1'))}/>
      <div id={'stopFlip'} onClick={() => context.stop(new RunningAnimation(AnimationType.Flip, 'id2'))}/>
      <div id={'stopPaging'} onClick={() => context.stop(new RunningAnimation(AnimationType.Paging, 'id3'))}/>
      <div id={'enable'} onClick={() => context.enable(true)}/>
      <div id={'disable'} onClick={() => context.enable(false)}/>
    </div>
  )}
  </AnimationBehaviorContext.Consumer>;

  it('fail when no children are provided', () => {
    expectToThrow(() => shallow(<AnimationBehaviorHandler/>),
      'Warning: Failed prop type: The prop `children` is marked as required in `AnimationBehaviorHandler`, but its value is `undefined`');
  });

  it('fail when AnimationsBehavior.NoHandler is provided', () => {
    expectToThrow(() => shallow(
      <AnimationBehaviorHandler behaviorType={AnimationsBehaviorType.NoHandler}>
        <div>child1</div>
      </AnimationBehaviorHandler>,
    ), 'Warning: Failed prop type: Invalid prop `behaviorType` of value `NoHandler` supplied to `AnimationBehaviorHandler`, ' +
      'expected one of ["NoAnimations","AllowAll","One","IndependentDrag"]');
  });

  it('renders children by default', () => {
    const wrapper = shallow(
      <AnimationBehaviorHandler>
        <div>child1</div>
        <div>child2</div>
      </AnimationBehaviorHandler>,
    );

    expect(wrapper.html()).toBe('<div>child1</div><div>child2</div>');
  });

  it('allows all animations at any time when AnimationsBehavior.AllowAll (default)', () => {
    const wrapper = mount(
      <AnimationBehaviorHandler>
        {TestAnimationBehaviorContextConsumer}
      </AnimationBehaviorHandler>,
    );
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    expect(wrapper.render().find('#dragRunning').html()).toBe('false');
    expect(wrapper.render().find('#flipRunning').html()).toBe('false');
    wrapper.find('#startFlip').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    expect(wrapper.render().find('#dragRunning').html()).toBe('false');
    expect(wrapper.render().find('#flipRunning').html()).toBe('true');
    wrapper.find('#startDrag').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    expect(wrapper.render().find('#dragRunning').html()).toBe('true');
    expect(wrapper.render().find('#flipRunning').html()).toBe('true');
    wrapper.find('#stopFlip').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    expect(wrapper.render().find('#dragRunning').html()).toBe('true');
    expect(wrapper.render().find('#flipRunning').html()).toBe('false');
    wrapper.find('#stopDrag').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    expect(wrapper.render().find('#dragRunning').html()).toBe('false');
    expect(wrapper.render().find('#flipRunning').html()).toBe('false');
  });

  it('throw miss use exceptions when AnimationsBehavior.AllowAll (default)', () => {
    const wrapper = mount(
      <AnimationBehaviorHandler>
        {TestAnimationBehaviorContextConsumer}
      </AnimationBehaviorHandler>,
    );
    // cannot stop when no animations running
    expectToThrow(() => wrapper.find('#stopDrag').simulate('click'),
      'Animation [Drag - id1] was not stopped. Animation was not running. Running Animations: []');
    expectToThrow(() => wrapper.find('#stopFlip').simulate('click'),
      'Animation [Flip - id2] was not stopped. Animation was not running. Running Animations: []');
    wrapper.find('#startDrag').simulate('click');

    // cannot stop when the received animation is not running but other animation is running
    expectToThrow(() => wrapper.find('#stopFlip').simulate('click'),
      'Animation [Flip - id2] was not stopped. Animation was not running. Running Animations: [Drag - id1]]');
  });

  it('allows only one animation at a time when AnimationsBehavior.One', () => {
    const wrapper = mount(
      <AnimationBehaviorHandler behaviorType={AnimationsBehaviorType.One}>
        {TestAnimationBehaviorContextConsumer}
      </AnimationBehaviorHandler>,
    );
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    wrapper.find('#startFlip').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('false');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('false');
    wrapper.find('#stopFlip').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    wrapper.find('#startDrag').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('false');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('false');
    wrapper.find('#stopDrag').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
  });

  it('throw miss use exceptions when AnimationsBehavior.One', () => {
    const wrapper = mount(
      <AnimationBehaviorHandler behaviorType={AnimationsBehaviorType.One}>
        {TestAnimationBehaviorContextConsumer}
      </AnimationBehaviorHandler>,
    );
    // cannot stop when no animations running
    expectToThrow(() => wrapper.find('#stopDrag').simulate('click'),
      'Animation [Drag - id1] was not stopped. Animation was not running. Running Animations: []');
    expectToThrow(() => wrapper.find('#stopFlip').simulate('click'),
      'Animation [Flip - id2] was not stopped. Animation was not running. Running Animations: []');

    wrapper.find('#startDrag').simulate('click');

    // cannot start more than one animation
    expectToThrow(() => wrapper.find('#startDrag').simulate('click'),
      'Animation [Drag - id1] was not run. Another animation is already executing: [Drag - id1]');
    expectToThrow(() => wrapper.find('#startFlip').simulate('click'),
      'Animation [Flip - id2] was not run. Another animation is already executing: [Drag - id1]');

    // cannot stop when the received animation is not running but other animation is running
    expectToThrow(() => wrapper.find('#stopFlip').simulate('click'),
      'Animation [Flip - id2] was not stopped. Animation was not running. Running Animations: [Drag - id1]');
  });

  it('dont allow other animations to run at the same time than drag animation when AnimationsBehavior.IndependentDrag', () => {
    const wrapper = mount(
      <AnimationBehaviorHandler behaviorType={AnimationsBehaviorType.IndependentDrag}>
        {TestAnimationBehaviorContextConsumer}
      </AnimationBehaviorHandler>,
    );
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    expect(wrapper.render().find('#pagingAllowed').html()).toBe('true');
    wrapper.find('#startFlip').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    expect(wrapper.render().find('#pagingAllowed').html()).toBe('true');
    wrapper.find('#startPaging').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    expect(wrapper.render().find('#pagingAllowed').html()).toBe('true');
    wrapper.find('#startDrag').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('false');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('false');
    expect(wrapper.render().find('#pagingAllowed').html()).toBe('false');
    wrapper.find('#stopFlip').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('false');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('false');
    expect(wrapper.render().find('#pagingAllowed').html()).toBe('false');
    wrapper.find('#stopDrag').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    expect(wrapper.render().find('#pagingAllowed').html()).toBe('true');
    wrapper.find('#stopPaging').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    expect(wrapper.render().find('#pagingAllowed').html()).toBe('true');
  });

  it('throw miss use exceptions when AnimationsBehavior.IndependentDrag', () => {
    const wrapper = mount(
      <AnimationBehaviorHandler behaviorType={AnimationsBehaviorType.One}>
        {TestAnimationBehaviorContextConsumer}
      </AnimationBehaviorHandler>,
    );
    // cannot stop when no animations running
    expectToThrow(() => wrapper.find('#stopDrag').simulate('click'),
      'Animation [Drag - id1] was not stopped. Animation was not running. Running Animations: []');
    expectToThrow(() => wrapper.find('#stopFlip').simulate('click'),
      'Animation [Flip - id2] was not stopped. Animation was not running. Running Animations: []');

    wrapper.find('#startDrag').simulate('click');

    // cannot start other animations while dragging
    expectToThrow(() => wrapper.find('#startDrag').simulate('click'),
      'Animation [Drag - id1] was not run. Another animation is already executing: [Drag - id1]');
    expectToThrow(() => wrapper.find('#startFlip').simulate('click'),
      'Animation [Flip - id2] was not run. Another animation is already executing: [Drag - id1]');

    // cannot stop when the received animation is not running but other animation is running
    expectToThrow(() => wrapper.find('#stopFlip').simulate('click'),
      'Animation [Flip - id2] was not stopped. Animation was not running. Running Animations: [Drag - id1]');
  });

  it('dont allow animations to run when AnimationsBehavior.NoAnimations', () => {
    const wrapper = mount(
      <AnimationBehaviorHandler behaviorType={AnimationsBehaviorType.NoAnimations}>
        {TestAnimationBehaviorContextConsumer}
      </AnimationBehaviorHandler>,
    );
    expect(wrapper.render().find('#dragAllowed').html()).toBe('false');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('false');
  });

  it('throw miss use exceptions when AnimationsBehavior.NoAnimations', () => {
    const wrapper = mount(
      <AnimationBehaviorHandler behaviorType={AnimationsBehaviorType.NoAnimations}>
        {TestAnimationBehaviorContextConsumer}
      </AnimationBehaviorHandler>,
    );
    // cannot stop when no animations running
    expectToThrow(() => wrapper.find('#stopDrag').simulate('click'),
      'Animation [Drag - id1] was not stopped. Animation was not running. Running Animations: []');
    expectToThrow(() => wrapper.find('#stopFlip').simulate('click'),
      'Animation [Flip - id2] was not stopped. Animation was not running. Running Animations: []');

    // cannot start animations
    expectToThrow(() => wrapper.find('#startDrag').simulate('click'),
      'Animation [Drag - id1] was not run. Animations are disabled');
    expectToThrow(() => wrapper.find('#startFlip').simulate('click'),
      'Animation [Flip - id2] was not run. Animations are disabled');
  });

  it('allows all animations without any exceptions when AnimationsBehavior.NoHandler. Default Context', () => {
    const wrapper = mount(
      <>
        <AnimationBehaviorContext.Consumer>{(context) => (
          <div>
            <div id={'dragAllowed'}>{context.allowed(AnimationType.Drag) ? 'true' : 'false'}</div>
            <div id={'dragRunning'}>{context.runningAny(AnimationType.Drag) ? 'true' : 'false'}</div>
            <div id={'startDrag'} onClick={() => context.start(new RunningAnimation(AnimationType.Drag, 'id1'))}/>
            <div id={'stopDrag'} onClick={() => context.stop(new RunningAnimation(AnimationType.Drag, 'id1'))}/>
            <div id={'disable'} onClick={() => context.enable(false)}/>
          </div>
        )}</AnimationBehaviorContext.Consumer>
      </>,
    );
    // Don't throw  exceptions stopping element not started
    wrapper.find('#stopDrag').simulate('click');

    // Allow all animations
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');

    // Don't throw exceptions starting element
    wrapper.find('#stopDrag').simulate('click');

    // Throw exception while disabling animations
    expectToThrow(() => wrapper.find('#disable').simulate('click'),
      'Create AnimationBehaviorHandler to handle animations state');

    // return false when checking running animations
    expect(wrapper.render().find('#dragRunning').html()).toBe('false');
  });

  it('throw miss use exception when not using AnimationBehaviorHandler and status is not AnimationsBehavior.NoHandler', () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      behaviorType: AnimationsBehaviorType.One,
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <AnimationBehaviorContext.Consumer>{(context) => (
          <div>
            <div id={'dragAllowed'} onClick={() => context.allowed(AnimationType.Drag)}/>
            <div id={'dragRunning'} onClick={() => context.runningAny(AnimationType.Drag)}/>
            <div id={'startDrag'} onClick={() => context.start(new RunningAnimation(AnimationType.Drag, 'id1'))}/>
            <div id={'stopDrag'} onClick={() => context.stop(new RunningAnimation(AnimationType.Drag, 'id1'))}/>
            <div id={'disable'} onClick={() => context.enable(false)}/>
          </div>
        )}</AnimationBehaviorContext.Consumer>
      </AnimationBehaviorContext.Provider>,
    );
    // cannot allow animations
    expectToThrow(() => wrapper.find('#dragAllowed').simulate('click'),
      'Create AnimationBehaviorHandler to handle animations state');

    // cannot start animations
    expectToThrow(() => wrapper.find('#startDrag').simulate('click'),
      'Create AnimationBehaviorHandler to handle animations state');

    // cannot stop animations
    expectToThrow(() => wrapper.find('#stopDrag').simulate('click'),
      'Create AnimationBehaviorHandler to handle animations state');

    // cannot disable animations
    expectToThrow(() => wrapper.find('#disable').simulate('click'),
      'Create AnimationBehaviorHandler to handle animations state');

    // cannot check running animations
    expectToThrow(() => wrapper.find('#dragRunning').simulate('click'),
      'Create AnimationBehaviorHandler to handle animations state');
  });

  it('should disable and enable animations with method', () => {
    const wrapper = mount(
      <AnimationBehaviorHandler>
        {TestAnimationBehaviorContextConsumer}
      </AnimationBehaviorHandler>,
    );
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
    wrapper.find('#disable').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('false');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('false');
    wrapper.find('#enable').simulate('click');
    expect(wrapper.render().find('#dragAllowed').html()).toBe('true');
    expect(wrapper.render().find('#flipAllowed').html()).toBe('true');
  });
});
