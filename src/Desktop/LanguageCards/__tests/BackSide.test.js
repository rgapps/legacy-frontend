import React from 'react';
import BackSide from '../BackSide';
import {shallow} from 'enzyme';
import '../../../__tests/setupTests';
import 'jest-styled-components';

describe('<BackSide />', () => {
  it('Should have 1 Child, (Word)', () => {
    const infoBack = {
      word: 'Code',
    };

    const wrapper = shallow(<BackSide back={infoBack}/>);
    const content = wrapper.children();

    expect(content.length).toBe(1);
    expect(content.at(0).text()).toBe('Code');
  });
});
