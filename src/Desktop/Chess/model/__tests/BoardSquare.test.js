import React from 'react';
import BoardSquare from '../BoardSquare';
import Promote from '../Promote';
import Piece from '../Piece';
import {mount} from 'enzyme';
import '../../../../__tests/setupTests';
import 'jest-styled-components';

describe('<BoardSquare />', () => {
  it('Should exists Piece if interactive is true and piece is not null', () => {
    const wrapper = mount(<BoardSquare interactive={true} piece={{type: 'p', color: 'b'}}/>);
    expect(wrapper.find(Piece).exists()).toBeTruthy();
  });

  it('Should not exists Piece if interactive is true and piece is null', () => {
    const wrapper = mount(<BoardSquare interactive={true} piece={null}/>);
    expect(wrapper.find(Piece).exists()).toBeFalsy();
  });

  it('Should not exists Piece if interactive is false', () => {
    const wrapper = mount(<BoardSquare interactive={false} piece={null} position={'a8'}/>);
    expect(wrapper.find(Piece).exists()).toBeFalsy();
  });

  it('Promote should exist if promotion', () => {
    const wrapper = mount(<BoardSquare promoting={{from: 'a7', to: 'a8', color: 'b'}} interactive={true} piece={null}
      position={'a8'}/>);

    expect(wrapper.find(Promote).exists()).toBeTruthy();
  });
});
