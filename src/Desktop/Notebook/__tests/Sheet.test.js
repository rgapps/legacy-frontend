import React from 'react';
import {mount, shallow} from 'enzyme';
import '../../../__tests/setupTests';
import Sheet, {Content} from '../Sheet';
import 'jest-styled-components';
import {expectToThrow, waitExpectWithAct} from '../../../__tests/setupTests';
import each from 'jest-each';
import {PageType} from '../model';

describe('<Sheet/>', () => {
  it('Fails when content is not configured', () => {
    expectToThrow(() => shallow(<Sheet/>),
      'Warning: Failed prop type: The prop `content` is marked as required in `Sheet`, but its value is `undefined`');
  });

  each([
    [{}, 'content.type'],
    [{type: PageType.DIRECT}, 'content.content'],
    [{type: PageType.EXTERNAL}, 'content.source'],
  ]).it('Fails with invalid content', (c, field) => {
    expectToThrow(() => shallow(<Sheet content={c}/>),
      `Warning: Failed prop type: The prop \`${field}\` is marked as required in \`Sheet\`, but its value is \`undefined\``);
  });

  const directContent = {type: PageType.DIRECT, content: 'cover content'};
  const externalContent = {type: PageType.EXTERNAL, source: 'http://sourceUrl.com'};

  it('renders with property direct content', () => {
    const wrapper = shallow(<Sheet content={directContent}/>);

    expect(wrapper.text()).toContain('cover content');
  });

  it('renders with property external content successful', async () => {
    const wrapper = mount(<Sheet content={externalContent}/>);

    expect(wrapper.find('iframe').prop('src')).toBe('http://sourceUrl.com');
  });

  it('redirect to external page if clicked', async () => {
    const wrapper = mount(<Sheet content={externalContent}/>);

    wrapper.find(Content).simulate('click');

    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith('http://sourceUrl.com');
    });
  });

  it('front style is set by default', () => {
    const wrapper = shallow(<Sheet content={directContent}/>);

    expect(wrapper).toHaveStyleRule('border-radius', '0 8px 8px 0');
  });

  it('back style is set if its back', () => {
    const wrapper = shallow(<Sheet content={directContent} back={true}/>);

    expect(wrapper).toHaveStyleRule('border-radius', '8px 0 0 8px');
  });
});
